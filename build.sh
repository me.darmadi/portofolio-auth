#/bin/sh

FILE=main.properties
if [ ! -f $FILE ]
then

cat > $FILE <<EOF
spring.main.allow-bean-definition-overriding=true
spring.cloud.config.name=auth-user
spring.cloud.config.uri=http://127.0.0.1:8888
EOF

fi

kill $(lsof -t -i :9797)
git pull origin master
mvn clean package

java -XX:+UseG1GC -Djava.awt.headless=true \
-Dspring.config.location=main.properties \
-jar target/auth-user-microservice-0.0.1.jar >./output.log 2>&1 &