@echo off
SET MAVEN_OPTS= -XX:+TieredCompilation -XX:TieredStopAtLevel=1
mvn -T 2C clean package
pause