@echo off
SET MAVEN_OPTS=-XX:+TieredCompilation -XX:TieredStopAtLevel=1
mvn  -T 4 -Dmaven.compiler.useIncrementalCompilation=false package