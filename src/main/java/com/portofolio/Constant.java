package com.portofolio;

public class Constant {

	public static boolean RESET_LAPORAN = false;

	public String kdDepartemen = null;
	public String kdPegawai = null;
	public String kdRuangan = null;
	public String namaLengkap = null;
	public String jenisKelamin = null;
	public Long tanggalLahir = null;
	public Integer noPegawai = 0;

	public static final String kdJenisPegawaiKeluarga = "00";
	public static final String kdJenisPegawai = "01";
	public String kdJabatan = "01";
	public int kdNegara = 1;
	public static final int kdKelompokUser = 1;
	public static final String kdKelompokPegawaiKeluarga = "0";
	public static final String kdKelompokPegawai = "1";
	public static final String kdModulAplikasi = "E2";

	public static final String namaJenisPegawaiKeluarga = "Keluarga Pegawai";
	public static final String namaJenisPegawai = "Administrator";

	public static final String namaKelompokPegawaiKeluarga = "Keluarga Pegawai";
	public static final String namaKelompokPegawai = "Staff";

	public static final String namaKelompokUserAdministrator = "Administrator";
	public static final String namaKelompokUserOperator = "Operator";

	public static final String namaJabatan = "Staff IT";
	public static final String namaRuangan = "Teknologi Informasi";
	public static final String namaDepartemen = "Teknologi Informasi";
	public static final String namaNegara = "Indonesia";
	
/////sequen counter
	public String kodeDepartemen = "1";
	public String kodeRuangan = "1";
	public String kodeKategoryPegawai = "1";
	public String kodeJabatan = "1";
	public Integer kodeTypePegawai = 1;
	public Integer kodePangkat = 1;
	public Integer kodeGolonganPegawai = 1; 
	public Integer kodeJenisBunga = 1;
	public Integer kodeJenisPajak = 1; 
	public Integer kodeJenisSukuBunga = 1;
	public Integer kodePekerjaan = 1;

	public static final String[] tSquence = { "Agama_M", "Profile_M", "Departemen_M", "Ruangan_M", "Pegawai_M",
			"JenisKelamin_M", "JenisPegawai_M", "KelompokUser_S", "Negara_M", "Jabatan_M", "Status_M", "Zodiak_M",
			"ZodiakUnsur_M", "JenisAlamat_M", "TitlePegawai_M", "JenisRange_M", "KelompokProduk_M", "JenisProduk_M",
			"DetailJenisProduk_M", "HubunganKeluarga_M", "KategoryDokumen_M", "JenisDokumen_M", "Dokumen_M",
			"Pendidikan_M", "PenghasilanTidakKenaPajak_M", "GolonganDarah_M" };
	public static final int[] tIdTerakhir = { 6, 4, 1, 1, 1, 2, 2, 2, 1, 1, 50, 12, 5, 3, 8, 11, 11, 11, 20, 22, 1, 13,
			8, 11, 12, 4 };

	public static final String[] Evict = { "AgamaDaoFindAllList", "AgamaDaoFindAllListPage", "AgamaDaofindOneBy",
			"AgamaDaoFindByKode", "AlatDaofindOneBy", "AlatDaoFindAllList", "AlatDaoFindAllListPage",
			"AlatDaoFindByKode", "AsalDaofindOneBy", "AsalDaoFindAllList", "AsalDaoFindAllListPage",
			"AsalDaoFindByKode", "AwardOffenceDaofindOneBy", "AwardOffenceDaoFindAllList",
			"AwardOffenceDaoFindAllListPage", "AwardOffenceDaoFindByKode", "BahanProdukDaofindOneBy",
			"BahanProdukDaoFindAllList", "BahanProdukDaoFindAllListPage", "bahanProdukDaoFindByKode",
			"CaraBayarDaofindOneBy", "CaraBayarDaoFindAllList", "CaraBayarDaoFindAllListPage", "CaraBayarDaoFindByKode",
			"DepartemenDaofindOneBy", "DepartemenDaoFindAllList", "DepartemenDaoFindAllListPage",
			"DepartemenDaoFindByKode", "DesaKelurahanDaofindOneBy", "DesaKelurahanDaoFindAllList",
			"DesaKelurahanDaoFindAllListPage", "DesaKelurahanDaoFindByKode", "DetailJenisProdukDaofindOneBy",
			"DetailJenisProdukDaoFindAllList", "DetailJenisProdukDaoFindAllListPage", "DetailJenisProdukDaoFindByKode",
			"DokumenDaofindOneBy", "DokumenDaoFindAllList", "DokumenDaoFindAllListPage", "DokumenDaoFindByKode" };

}
