	package com.portofolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
*
* @author >
*/

@SpringBootApplication
//@EnableDiscoveryClient
//@EnableEurekaClient
//@EnableCaching
@EnableScheduling
public class PortofolioAuthMain  {
	public static void main(String[] args) {
		SpringApplication.run(PortofolioAuthMain.class, args);
	}
}
