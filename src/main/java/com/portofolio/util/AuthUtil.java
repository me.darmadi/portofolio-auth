package com.portofolio.util;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.portofolio.dto.AuthDto;
import com.portofolio.security.TokenAuthenticationService;
import com.portofolio.security.UserAuthentication;
import com.portofolio.service.AuthService;

/**
 *
 * @author >
 */

public class AuthUtil {

	public static Map<String, Object> GrandAuth(AuthDto authDto, AuthService authService,
			TokenAuthenticationService tokenAuthenticationService, HttpServletResponse httpResponse) {
		authDto = authService.authorize(authDto);
		
		String token = "";
		
		if (authDto != null) {
			String sessString = authService.toSessionJson(authDto);

			GrantedAuthority authority = new SimpleGrantedAuthority("USER");
			token = tokenAuthenticationService.addAuthentication(httpResponse,
					new UserAuthentication(new User(sessString, authDto.getEncrypted(), Arrays.asList(authority))));

			Map<String, Object> map = CommonUtil.createMap();
			
			map.put("token", token);
			map.put("dto", authDto);
			
			return map;
			
		}
		return null;
	}
}
