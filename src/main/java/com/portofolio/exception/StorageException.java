package com.portofolio.exception;

public class StorageException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7884228681064640342L;

	public StorageException() {
		super();
	}
	
	public StorageException(String message) {
		 super(message);
	}
	
	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
