/*
 * package com.portofolio.resource.controller;
 * 
 * import java.io.File; import java.util.List;
 * 
 * import javax.servlet.http.HttpServletResponse;
 * 
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.core.io.Resource; import
 * org.springframework.http.HttpHeaders; import
 * org.springframework.http.MediaType; import
 * org.springframework.http.ResponseEntity; import
 * org.springframework.util.FileCopyUtils; import
 * org.springframework.web.bind.annotation.GetMapping; import
 * org.springframework.web.bind.annotation.PathVariable; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.RequestParam; import
 * org.springframework.web.bind.annotation.ResponseBody; import
 * org.springframework.web.bind.annotation.RestController; import
 * org.springframework.web.multipart.MultipartFile; import
 * org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
 * import org.springframework.web.servlet.mvc.support.RedirectAttributes;
 * 
 * import com.portofolio.exception.InfoException; import
 * com.portofolio.resource.service.StorageService; import
 * com.portofolio.security.AppPermission; import com.portofolio.util.CommonUtil;
 * 
 * import io.swagger.annotations.ApiOperation;
 * 
 * @RestController
 * 
 * @RequestMapping("/file") public class FileProsesController {
 * 
 * Logger log = LoggerFactory.getLogger(FileProsesController.class);
 * 
 * @Autowired private StorageService storageService;
 * 
 * @ApiOperation(value = "Api Untuk Mengunduh File", produces =
 * "application/pdf, application/octet-stream", response = File.class)
 * 
 * @AppPermission(AppPermission.VIEW)
 * 
 * @GetMapping(path = "/download/{filename:.+}")
 * 
 * @ResponseBody public ResponseEntity<StreamingResponseBody>
 * serveFile(@PathVariable String filename,
 * 
 * @RequestParam(value = "download", required = false, defaultValue = "true")
 * boolean download, HttpServletResponse response){ Resource file =
 * storageService.loadAsResource(filename); // return
 * ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
 * "attachment; filename=\"" + file.getFilename() + "\"").body(file.getFile());
 * MediaType type =
 * filename.endsWith("pdf")?MediaType.APPLICATION_PDF:MediaType.
 * APPLICATION_OCTET_STREAM; //response.setContentType(type); if (download) {
 * response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
 * + file.getFilename() + "\""); } try { //return
 * FileCopyUtils.copyToByteArray(file.getInputStream());
 * 
 * StreamingResponseBody stream = out ->
 * FileCopyUtils.copy(file.getInputStream(), out); return ResponseEntity.ok()
 * .contentType(type) .body(stream);
 * 
 * }catch(Exception e) { throw new InfoException("File " + filename +
 * " tidak bisa dibaca."); } }
 * 
 * @ApiOperation(value = "Api Untuk Menyimpan File", consumes =
 * "image/png, image/jpg, image/gif, image/jpeg, application/pdf, application/octet-stream"
 * , produces = MediaType.TEXT_PLAIN_VALUE)
 * 
 * @AppPermission(AppPermission.SPECIALS)
 * 
 * @PostMapping(path = "/upload", produces = MediaType.TEXT_PLAIN_VALUE)
 * 
 * @ResponseBody public ResponseEntity<String>
 * handleFotoUpload(@RequestParam("file") MultipartFile file, RedirectAttributes
 * redirectAttributes) { storageService.init(); File simpan =
 * storageService.store(file); redirectAttributes.addFlashAttribute("message",
 * "Berhasil di upload " + file.getOriginalFilename() + "!");
 * log.warn("Berhasil di upload " + file.getOriginalFilename() + "!"); return
 * ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(simpan.getName());
 * // return simpan.getName(); }
 * 
 * 
 * }
 */