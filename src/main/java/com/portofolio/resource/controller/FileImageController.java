package com.portofolio.resource.controller;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.portofolio.exception.ApiError;
import com.portofolio.exception.InfoException;
import com.portofolio.resource.service.StorageService;
import com.portofolio.security.AppPermission;
import com.portofolio.util.RestUtil;
import com.portofolio.util.SmallUtil;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/image")
public class FileImageController {

	@Autowired
	private StorageService storageService;

	@ApiOperation(value = "Api Untuk Menyimpan File", consumes = "image/png, image/jpg, image/gif, image/jpeg, application/pdf, application/octet-stream", produces = MediaType.TEXT_PLAIN_VALUE)
	@AppPermission(AppPermission.SPECIALS)
	@PostMapping(path = "/upload", produces = MediaType.TEXT_PLAIN_VALUE)
	@ResponseBody
	public ResponseEntity<Map<String, Object>>  handleFotoUpload(@RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes) {
		storageService.init();
		Map<String,Object> result = storageService.store(file);
		redirectAttributes.addFlashAttribute("message", "Berhasil di upload " + file.getOriginalFilename() + "!");
		return RestUtil.getJsonResponse(result);
//    return simpan.getName();
	}

	@ApiOperation(value = "Api Untuk Melihat File Gambar", produces = "image/png, image/jpg, image/gif, image/jpeg")
	@AppPermission(AppPermission.VIEW)
	@GetMapping(path = "/show/{filename:.+}", produces = { MediaType.IMAGE_PNG_VALUE, MediaType.IMAGE_JPEG_VALUE,
			MediaType.IMAGE_GIF_VALUE })
	@ResponseBody
	public byte[] showFile(@PathVariable String filename, HttpServletResponse response) {
		Resource file = storageService.loadAsResource(filename);
		String type = filename.endsWith("png") ? MediaType.IMAGE_PNG_VALUE
				: filename.endsWith("gif") ? MediaType.IMAGE_GIF_VALUE : MediaType.IMAGE_JPEG_VALUE;
//		response.setHeader("Content-Type", "image/*");
		response.setContentType(type);
		try {
			return FileCopyUtils.copyToByteArray(file.getInputStream());
		} catch (Exception e) {
			throw new InfoException("File " + filename + " tidak bisa dibaca.");
		}
	}

	@ExceptionHandler(InfoException.class)
	public ResponseEntity<?> handleInfoException(InfoException ex) {

		final StringBuilder builder = new StringBuilder();

		ApiError apiError = new ApiError();

		List<Map<String, Object>> data = SmallUtil.createList();
		Map<String, Object> rm = SmallUtil.createMap();

		builder.append(ex.toString());
		String replac = builder.toString();
		replac = replac.replace("com.portofolio.exception.InfoException:", "");
		rm.put("error", replac);
		data.add(rm);
		apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, "501");

		SmallUtil.outPrintln(ex + ":" + ex.getClass().getName());
		SmallUtil.outPrintln(ex + ":" + ex.getCause());
		SmallUtil.showFive(ex);

		return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
}
