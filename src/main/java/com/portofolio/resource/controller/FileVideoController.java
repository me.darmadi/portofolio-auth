//package com.portofolio.resource.controller;
//
//import java.util.List;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.Resource;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.FileCopyUtils;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.portofolio.exception.ApiError;
//import com.portofolio.exception.InfoException;
//import com.portofolio.resource.service.StorageService;
//import com.portofolio.security.AppPermission;
//import com.portofolio.util.SmallUtil;
//
//import io.swagger.annotations.ApiOperation;
//
//@Controller
//@RequestMapping("/video")
//public class FileVideoController {
//
//	@Autowired
//	private StorageService storageService;
//
//	
//	@ApiOperation(value = "Api Untuk Melihat File Video", 
//			 produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
//	@AppPermission(AppPermission.VIEW)
//	@GetMapping(path = "/show/{filename:.+}")
//	@ResponseBody
//	public byte[] showFile(@PathVariable String filename,  
//			@RequestParam(value = "noProfile", required = false, defaultValue = "true") Boolean noProfile, // Nomor Profile ada
//			@RequestParam(value = "kdProfile", required = false, defaultValue = "-1000") Integer kdProfile,
//			HttpServletResponse response)  {
//		Resource file = storageService.loadAsResource(filename);		
//		//String type = filename.endsWith("mp4")?"video/mp4":filename.endsWith("flv")?"video/x-flv":"video/x-ms-wmv";
//		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
//		try {
//			//final byte[] ob = FileCopyUtils.copyToByteArray(file.getInputStream());
//			//StringUtil.outPrintln("ukuran file " + ob.length);
//			
//			//StreamingResponseBody stream = out -> FileCopyUtils.copy(file.getInputStream(), out);
//			
//			//return stream;
//			
//			return FileCopyUtils.copyToByteArray(file.getInputStream());
//			
////			return new StreamingResponseBody() {
////
////				@Override
////				public void writeTo(OutputStream out) throws IOException {
////					BufferedOutputStream bos = new BufferedOutputStream(out);
////					for (int i = 0; i< ob.length; i++) {
////						bos.write(ob[i]);
////						if ((i % 2048) == 0) {
////							bos.flush();
////						}
////					}
////					bos.flush();
////				}
////				
////			};
//			//FileCopyUtils.copy(ob, new BufferedOutputStream(response.getOutputStream()));
//			
////			return ResponseEntity.ok()
////			        .contentType(type)
////			        .body(stream);
//			
//		}catch(Exception e) {
//			e.printStackTrace();
//			throw new InfoException("File " + filename + " tidak bisa dibaca.");
//		}
//	}
//
//
////	@ApiOperation(value = "Api Untuk Melihat File Video", 
////			produces = "video/x-flv, video/mp4, video/x-ms-wmv")
////	@AppPermission(AppPermission.VIEW)
////	@GetMapping(path = "/showVideos/{filename:.+}", 
////			produces = {
////					"video/x-flv",
////					"video/mp4",
////					"video/x-ms-wmv"
////					}
////	)
////	@ResponseBody
////	public byte[] showFileVideos(@PathVariable String filename, 
////			HttpServletResponse response)  {
////		Resource file = storageService.loadAsResource2ndStorage(filename);		
////		String type = filename.endsWith("mp4")?"video/mp4":filename.endsWith("flv")?"video/x-flv":"video/x-ms-wmv";
////		response.setContentType(type);
////		try {
////			return FileCopyUtils.copyToByteArray(file.getInputStream());
////		}catch(Exception e) {
////			throw new InfoException("File " + filename + " tidak bisa dibaca.");
////		}
////	}
//
//    @ExceptionHandler(InfoException.class)
//    public ResponseEntity<?> handleInfoException(InfoException ex) {
//    	
//    	final StringBuilder builder = new StringBuilder();
//    	
//    	ApiError apiError = new ApiError();
//    	
//    	List<Map<String,Object>> data = SmallUtil.createList();
//        Map<String,Object> rm = SmallUtil.createMap();
//    	
//		builder.append(ex.toString());
//		String replac=builder.toString();
//		replac=replac.replace("com.portofolio.exception.InfoException:", "");
//        rm.put("error",replac);
//        data.add(rm);
//		apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR", data, "501");
//		
//    	SmallUtil.outPrintln(ex + ":" +ex.getClass().getName());
//    	SmallUtil.outPrintln(ex + ":" + ex.getCause());
//		SmallUtil.showFive(ex);
//    	
//        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
//    }
//}
