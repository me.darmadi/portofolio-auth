package com.portofolio.resource.service;

import java.util.List;
import java.util.Map;

public interface JenisProfileService {

	public List<Map<String, Object>> getAll();
	
}
