package com.portofolio.resource.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Stream;

public interface StorageService {
	void init();
	File storeReportFile(MultipartFile file);
    Stream<Path> loadAll(Boolean noProfile, Integer kdProfile);
    Path load(String filename);
    Resource loadAsResource(String filename);
    void deleteAll();
    Integer getKdProfile(); 
	Resource loadAsResource2ndStorage(String filename);
	Path load2ndStorage(String filename);
	Map<String, Object> store(MultipartFile file);

}
