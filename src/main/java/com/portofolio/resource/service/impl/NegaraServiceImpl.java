package com.portofolio.resource.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.dao.NegaraDao;
import com.portofolio.resource.service.NegaraService;

@Lazy
@Service
public class NegaraServiceImpl implements NegaraService {

	@Autowired
	private NegaraDao negaraDao;

	@Override
	public List<Map<String, Object>> getAll() { 
		return negaraDao.findAllList();
	}

}
