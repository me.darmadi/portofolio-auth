package com.portofolio.resource.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.dao.JenisProfileDao;
import com.portofolio.resource.service.JenisProfileService;

@Service("JenisProfileService")
public class JenisProfileServiceImpl implements JenisProfileService {
	
	@Autowired JenisProfileDao jenisProfileDao;

	public List<Map<String, Object>> getAll(){
		return jenisProfileDao.findAllJenisProfile();
	}
}
