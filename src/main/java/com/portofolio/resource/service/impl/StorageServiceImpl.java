package com.portofolio.resource.service.impl;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.ProfileDao;
import com.portofolio.exception.InfoException;
import com.portofolio.helper.StorageProperties;
import com.portofolio.resource.service.StorageService;
import com.portofolio.service.SessionService;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.StringUtil;

@Service("StorageService")
public class StorageServiceImpl extends BaseServiceImpl implements StorageService {

	// private final Path rootLocation;

	private String rootDir;
	private String rootDirReport;
	private String rootDirPhotos;

	@Autowired
	@Qualifier("SessionService")
	SessionService sessionService;

	@Autowired
	ProfileDao profileDao;

	@Autowired
	public StorageServiceImpl(StorageProperties properties) {
		// this.rootLocation = Paths.get(properties.getLocation());
		this.rootDir = properties.getLocation();
		this.rootDirReport = properties.getLocationReport();
		this.rootDirPhotos = properties.getLocationPhotos();
	}

	@Override
	public Integer getKdProfile() {
		try {
			return sessionService.getSession().getKdProfile();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public File storeReportFile(MultipartFile file) {
		Path rootLocation = Paths.get(rootDirReport);
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		Path path;

		try {
			if (file.isEmpty()) {
				throw new InfoException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				throw new InfoException("Cannot store file with relative path outside current directory " + filename);
			}
			path = rootLocation.resolve(filename);
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new InfoException("Failed to store file " + filename, e);
		}

		return path.toFile();
	}

	@Override
	public Map<String, Object> store(MultipartFile file) {
		result.clear();
		Boolean resizeImage = false;
		String tambahan = "/profile-" + getKdProfile();

		Path rootLocation = Paths.get(rootDir + tambahan);

		String filename = StringUtils.cleanPath(file.getOriginalFilename());

		if (filename.indexOf('.') > -1) {
			filename = filename.substring(0, filename.indexOf('.')) + (System.currentTimeMillis() / 1000)
					+ filename.substring(filename.indexOf('.'));
		} else {
			filename += (System.currentTimeMillis() / 1000);
		}

		Path path;
		try {
			if (file.isEmpty()) {
				throw new InfoException("Failed to store empty file " + filename);
			}
			if (filename.contains("..")) {
				throw new InfoException("Cannot store file with relative path outside current directory " + filename);
			}
			path = rootLocation.resolve(filename);
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new InfoException("Failed to store file " + filename, e);
		}

		File f = path.toFile();
		if (resizeImage) {
			try {
				resize(352, 240, f);
			} catch (Exception e) {
			}
		}

		try {
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
			result.put("fileName", filename);
			resultSuccessful();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			resultError(e);
		}

		return result;
	}

	private void resize(int width, int height, File pathImage) throws Exception {

		FileInputStream fis = new FileInputStream(pathImage);
		BufferedImage sourceImage = ImageIO.read(fis);
		if (sourceImage.getSampleModel().getWidth() < width || sourceImage.getSampleModel().getHeight() < height) {
			return;
		}
		Image thumbnail = sourceImage.getScaledInstance(-1, height, Image.SCALE_SMOOTH);
		BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null), thumbnail.getHeight(null),
				BufferedImage.TYPE_INT_RGB);
		bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
		fis.close();
		FileOutputStream fos = new FileOutputStream(pathImage);
		ImageIO.write(bufferedThumbnail, "jpeg", fos);

		// System.out.println(pathImage);
	}

	@Async
	public void copyToAll(long total, String rootDir, MultipartFile file, String filename) {
		for (int i = 0; i < 1; i++) {
			try {

				String tambahan = "/profile-" + getKdProfile();
				Path rootLocation = Paths.get(rootDir + tambahan);
				Files.createDirectories(rootLocation);
				Path path = rootLocation.resolve(filename);
				Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);

			} catch (Exception e) {
				StringUtil.printStackTrace(e);
			}
		}
	}

	@Override
	public Stream<Path> loadAll(Boolean noProfile, Integer kdProfile) {
		String tambahan = "";
		if (CommonUtil.isNotNullOrEmpty(getKdProfile()) && noProfile) {
			tambahan = "/profile-" + getKdProfile();
		}

		if (kdProfile != -1000) {
			tambahan = "/profile-" + kdProfile;
		}

		Path rootLocation = Paths.get(rootDir + tambahan);
		try {
			return Files.walk(rootLocation, 3).filter(path -> !path.equals(rootLocation))
					.map(path -> rootLocation.relativize(path));
		} catch (IOException e) {
			throw new InfoException("Failed to read file from storage", e);
		}

	}

	@Override
	public Path load(String filename) {
		String tambahan = "";

		tambahan = "/profile-" + getKdProfile();

		Path rootLocation = Paths.get(rootDir + tambahan);
		return rootLocation.resolve(filename);
	}

	@Override
	public Path load2ndStorage(String filename) {
		Path rootLocation = Paths.get(rootDirPhotos);
		return rootLocation.resolve(filename);
	}

	@Override
	public Resource loadAsResource(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new InfoException("File tidak ditemukan: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new InfoException("Cek nama direktori upload sudah benar: " + filename, e);
		}
	}

	@Override
	public Resource loadAsResource2ndStorage(String filename) {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new InfoException("File tidak ditemukan: " + filename);
			}
		} catch (MalformedURLException e) {
			throw new InfoException("Cek nama direktori upload sudah benar: " + filename, e);
		}
	}

	@Override
	public void deleteAll() {
		Path rootLocation = Paths.get(rootDir + "/profile-" + getKdProfile());
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	@Override
	public void init() {
		Path rootLocation = Paths.get(rootDir + "/profile-" + getKdProfile());
		try {
			Files.createDirectories(rootLocation);
		} catch (IOException e) {
			throw new InfoException("Gagal membuat direktori", e);
		}
	}
}
