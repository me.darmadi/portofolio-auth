package com.portofolio.dao.provider;

import java.util.Map;

import com.portofolio.domain.MapObjekModulToModulAplikasi;
import com.portofolio.domain.ObjekModulAplikasi;
import com.portofolio.helper.DBUtil;;

public class AppMenuDinamicProvider {
	
	public String queryAllObjekAplikasi(Map<String, Object> parameters) {
		String sb = "SELECT "
				+ DBUtil.extract(ObjekModulAplikasi.class, "A")
				+ ","
				+ DBUtil.extract(MapObjekModulToModulAplikasi.class, "B")
				+ " FROM " + ObjekModulAplikasi.table() + " A "
				+ " LEFT JOIN " + MapObjekModulToModulAplikasi.table() + " B "
				+ " ON A.KdObjekModulAplikasi = B.KdObjekModulAplikasi "
				+ " WHERE B.KdProfile = #{kdProfile} "
				+ " AND B.KdModulAplikasi = #{kdModulAplikasi} "
				+ " AND A.StatusEnabled = 1 "
				+ " AND B.StatusEnabled = 1 "
				+ " ORDER BY B.NoUrutObjek ASC ";
		
		return sb;
	}

	public String queryAwalObjekAplikasi(Map<String, Object> parameters) {
		String sb = "SELECT "
				+ DBUtil.extract(ObjekModulAplikasi.class, "A")
				+ ","
				+ DBUtil.extract(MapObjekModulToModulAplikasi.class, "B")
				+ " FROM " + ObjekModulAplikasi.table() + " A "
				+ " LEFT JOIN " + MapObjekModulToModulAplikasi.table() + " B "
				+ " ON A.KdObjekModulAplikasi = B.KdObjekModulAplikasi "
				+ " WHERE B.KdProfile = #{kdProfile} "
				+ " AND B.KdModulAplikasi = #{kdModulAplikasi} "
				+ " AND B.KdObjekModulAplikasiHead IS NULL "
				+ " AND A.StatusEnabled = 1 "
				+ " AND B.StatusEnabled = 1 "
				+ " ORDER BY B.NoUrutObjek ASC ";
		
		return sb;
	}

	public String queryLanjutObjekAplikasi(Map<String, Object> parameters) {
		String sb = "SELECT "
				+ DBUtil.extract(ObjekModulAplikasi.class, "A")
				+ ","
				+ DBUtil.extract(MapObjekModulToModulAplikasi.class, "B")
				+ " FROM " + ObjekModulAplikasi.table() + " A "
				+ " LEFT JOIN " + MapObjekModulToModulAplikasi.table() + " B "
				+ " ON A.KdObjekModulAplikasi = B.KdObjekModulAplikasi "
				+ " WHERE B.KdProfile = #{kdProfile} "
				+ " AND B.KdModulAplikasi = #{kdModulAplikasi} "
				+ " AND B.KdObjekModulAplikasiHead IS #{kdObjekModulAplikasiHead} "
				+ " AND A.StatusEnabled = 1 "
				+ " AND B.StatusEnabled = 1 "
				+ " ORDER BY B.NoUrutObjek ASC ";
		
		return sb;
	}
}
