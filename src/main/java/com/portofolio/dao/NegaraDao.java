package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Negara;
import com.portofolio.entity.vo.NegaraId;

@Repository
public interface NegaraDao extends CrudRepository<Negara, NegaraId>  {
	 
    @Query("select new map(model.kode as kdNegara "
            + ", model.namaNegara as namaNegara )" 
            + " from Negara model where model.statusEnabled = true ")
    List<Map<String, Object>> findAllList();
  
 
}
