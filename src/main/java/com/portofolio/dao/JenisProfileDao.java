package com.portofolio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.JenisProfile;

@Repository
public interface JenisProfileDao extends CrudRepository<JenisProfile, Integer>  {

	@Query(" Select "
			+ " new map("
			+ " model.kdJenisProfile as kdJenisProfile, "
			+ " model.namaJenisProfile as namaJenisProfile "
			+ " ) "
			+ " from JenisProfile model "
			+ " where model.statusEnabled = true ")
	List<Map<String, Object>> findAllJenisProfile();


}
