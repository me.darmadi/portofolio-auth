package com.portofolio.dao;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.Profile;
import com.portofolio.entity.vo.ProfileId;

@Repository("profileDao")
public interface ProfileDao extends JpaRepository<Profile, ProfileId> {

	@Cacheable("GambarLogo")
	@Query("select model.gambarLogo as gambarLogo " + " from Profile model " + " where  model.kode = :kdProfile ")
	String findGambarLogo(@Param("kdProfile") Integer kdProfile);

	@Cacheable("NamaProfile")
	@Query("select model.namaLengkap as namaLengkap " + " from Profile model " + " where  model.kode = :kdProfile ")
	String findNamaProfile(@Param("kdProfile") Integer kdProfile);


}
