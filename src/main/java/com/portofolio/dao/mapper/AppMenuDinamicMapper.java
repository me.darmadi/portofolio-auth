package com.portofolio.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.cache.annotation.Cacheable;

import com.portofolio.auth.dto.MenuDto;
import com.portofolio.dao.provider.AppMenuDinamicProvider;
/**
*
* @author >
*/
@Mapper
public interface AppMenuDinamicMapper {

	@SelectProvider(type = AppMenuDinamicProvider.class, method = "queryAllObjekAplikasi")
	@Cacheable("AppMenuDinamicMapper1")
	List<MenuDto> selectAllObjekAplikasi(@Param("kdProfile") Integer kdProfile,
			@Param("kdModulAplikasi") String kdModulAplikasi) throws RuntimeException;
	
	@SelectProvider(type = AppMenuDinamicProvider.class, method = "queryAwalObjekAplikasi")
	@Cacheable("AppMenuDinamicMapper2")
	List<MenuDto> selectAwalObjekAplikasi(@Param("kdProfile") Integer kdProfile,
			@Param("kdModulAplikasi") String kdModulAplikasi) throws RuntimeException;
	
	@SelectProvider(type = AppMenuDinamicProvider.class, method = "queryLanjutObjekAplikasi")
	@Cacheable("AppMenuDinamicMapper3")
	List<MenuDto> selectLanjutObjekAplikasi(@Param("kdProfile") Integer kdProfile,
			@Param("kdModulAplikasi") String kdModulAplikasi,
			@Param("kdObjekModulAplikasiHead") String kdObjekModulAplikasiHead) throws RuntimeException;
}
