package com.portofolio.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.portofolio.entity.MapModulAplikasiToObjekModul;
import com.portofolio.entity.ObjekModulAplikasi;
import com.portofolio.entity.vo.MapModulAplikasiToObjekModulId;

@Repository
public interface MapModulAplikasiToObjekModulDao extends CrudRepository<MapModulAplikasiToObjekModul, MapModulAplikasiToObjekModulId> {

	String QSelectAll = "select model "
			+ " from ObjekModulAplikasi model "
			+ " left join model.mapObjekModulToKelompokUser mkUser "
			+ " left join model.mapModulAplikasiToObjekModul mmApp "
			+ " where mkUser.id.kdProfile = :kdProfile "
			+ " and mkUser.id.kdVersion = mmApp.id.kdVersion "
			+ " and mkUser.id.kdModulAplikasi = mmApp.id.kdModulAplikasi "
			+ " and mmApp.id.kdModulAplikasi = :kdModulAplikasi "
			+ " and mmApp.id.kdVersion = :kdVersion "
			+ " and mkUser.id.kdKelompokUser = :kdKelompokUser "
			+ " and mkUser.tampil = 1 "
			+ " and model.alamatURLFormObjek is not null "
			+ " and model.statusEnabled = true "
			+ " and mmApp.statusEnabled = true " 
			+ " and mkUser.statusEnabled = true";

	@Query(QSelectAll + " and model.kdObjekModulAplikasiHead = :kdObjekModulAplikasiHead ")
	List<ObjekModulAplikasi> findAllObjekModulAplikasiDaofindMenu(
			@Param("kdProfile") Integer kdProfile, 
			@Param("kdModulAplikasi") String kdModulAplikasi,
			@Param("kdVersion") Integer kdVersion, 
			@Param("kdKelompokUser") Integer kdKelompokUser,
			@Param("kdObjekModulAplikasiHead") String kdObjekModulAplikasiHead);
	
	@Query(QSelectAll + " and model.kdObjekModulAplikasiHead is null ")
	List<ObjekModulAplikasi> findAllObjekModulAplikasiDaofindMenu(
			@Param("kdProfile") Integer kdProfile, 
			@Param("kdModulAplikasi") String kdModulAplikasi,
			@Param("kdVersion") Integer kdVersion, 
			@Param("kdKelompokUser") Integer kdKelompokUser);
	
	
	String QSelectAllAdmin = "select model "
			+ " from ObjekModulAplikasi model "
			+ " left join model.mapModulAplikasiToObjekModul mmApp "
			+ " where mmApp.id.kdModulAplikasi = :kdModulAplikasi "
			+ " and mmApp.id.kdVersion = :kdVersion "
			+ " and model.alamatURLFormObjek is not null "
			+ " and model.statusEnabled = true "
			+ " and mmApp.statusEnabled = true " ;

	@Query(QSelectAllAdmin + " and model.kdObjekModulAplikasiHead = :kdObjekModulAplikasiHead ")
	List<ObjekModulAplikasi> findAllObjekModulAplikasiDaofindMenuAdmin(
			@Param("kdVersion") Integer kdVersion, 
			@Param("kdModulAplikasi") String kdModulAplikasi,
			@Param("kdObjekModulAplikasiHead") String kdObjekModulAplikasiHead);
	
	@Query(QSelectAllAdmin + " and model.kdObjekModulAplikasiHead is null ")
	List<ObjekModulAplikasi> findAllObjekModulAplikasiDaofindMenuAdmin(
			@Param("kdVersion") Integer kdVersion, 
			@Param("kdModulAplikasi") String kdModulAplikasi);
	
}
