package com.portofolio.lang.dto;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageManagerDto {
	
	
	@NotNull(message = "LanguageManagerDto.kdModulAplikasi.notnull")
	String kdModulAplikasi;
	
	@NotNull(message = "LanguageManagerDto.kdBahasa.notnull")
	Integer kdBahasa;
	
	@NotNull(message = "LanguageManagerDto.kdProfile.notnull")
	Integer kdProfile;
	
	@NotNull(message = "LanguageManagerDto.version.notnull")
	Integer kdVersion;
	
	String lang;
	//Map<String, Object> label; 
	
	@Valid
	Set<LanguageManagerDetailDto> detailDto;
	
}
