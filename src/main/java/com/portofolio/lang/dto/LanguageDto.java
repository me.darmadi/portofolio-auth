package com.portofolio.lang.dto;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageDto {
	String id;
	Map<String, Object> label; 
}
