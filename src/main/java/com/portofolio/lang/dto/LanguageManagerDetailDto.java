package com.portofolio.lang.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageManagerDetailDto {
	
	@NotNull(message = "LanguageManagerDetailDto.key.notnull")
	String key;
	@NotNull(message = "LanguageManagerDetailDto.value.notnull")
	String value;
}
