package com.portofolio.report.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericCustomReportDto {
	
	String namaFile;
	String extFile;
	Boolean excelOneSheet;
	
	Boolean download;
	
	Boolean outProfile;
	Integer kdProfile;

	Boolean outDepartemen;
	String kdDepartemen;
	
	String[] paramKey;
	Object[] paramValue;
	
	String[] paramImgKey;
	String[] paramImgValue;
	
	String jsonStringfy;
	
}
