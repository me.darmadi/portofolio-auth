package com.portofolio.report.helper;

import java.awt.Image;
import java.io.File;
import java.util.Map;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import com.portofolio.dao.ProfileDao;
import com.portofolio.exception.InfoException;
import com.portofolio.report.base.ReportBase;
import com.portofolio.service.LoginUserService;
import com.portofolio.util.CommonUtil;

import ar.com.fdvs.dj.domain.DynamicReport;
import lombok.Getter;
import lombok.Setter;

/**
*
* @author >
*/

@Getter
@Setter
public abstract class HelperReportBase extends ReportBase {

	@Value("${info.resource:http://192.168.0.143:9191}")
	protected String urlAkses;
	
	@Value("${info.upload-foto}")
	protected String location = "D:/WORKSPACE/Framework/upload-dir";

	//@Value("${info.upload-foto-local}")
	protected String locationLocal = "D:/upload-dir";
	
	
	@Autowired ProfileDao profileDao;
	@Autowired LoginUserService loginUserService;
	
	
	@Override
	public DynamicReport getDynamicReport() {
		return null;
	}
	
	@Override
	public boolean isJpa() {
		return false;
	}
	
	protected Map<String, Object> m;
	
	public void setValue(String[] key, Object[] val) {
		m = CommonUtil.createMap();		
		if (key != null && val != null) {
			if (key.length != val.length) {
				throw new RuntimeException("Panjang Parameter dan Value tidak sama");				
			}
			for (int i=0; i<key.length; i++) {
				m.put(key[i], val[i]);
			}
		}
	}
	
	public void setImageValue(String[] key, String[] val) {
		if (key != null && val != null && key.length == val.length) {
			if (key.length != val.length) {
				throw new RuntimeException("Panjang Parameter Img dan Value Img tidak sama");				
			}
			for (int i=0; i<key.length; i++) {
				m.put(key[i], getImage(val[i]));
			}
		}
	}

	//protected String gambarLogo;
	
	//public abstract void setGambarLogo(String gambarLogo);
	
	protected boolean outProfile = false;	
	protected Integer kdProfile;

	protected boolean outDepartemen = false;	
	protected String kdDepartemen;
	
	public Integer getKdProfile() {
		if (outProfile) {
			return kdProfile;
		} else {
			return loginUserService.getSession().getKdProfile();
		}
	}
	
	public String getKdDepartemen() {
		if (outDepartemen) {
			return kdDepartemen;
		} else {
			return loginUserService.getSession().getKdDepartemen();
		}
	}

	public String getKdPegawai() {
		return loginUserService.getSession().getKdPegawai();
	}
	
	
	
	public String getNamaProfile() {
		return profileDao.findNamaProfile(getKdProfile());
	}
		
	public String getGambarLogo() {
		return profileDao.findGambarLogo(getKdProfile());
	}
	
	public String getFolderClasspath() {
		return "classpath:/report/";
	}
	
	public Image getImage(String namafile) {
		
		if (namafile == null || "null".equals(namafile.trim()) || "".equals(namafile.trim())) {
			namafile = "noimage.jpg";
		}
		
		String curLoc = location;
		//agar bisa ditest langsung di local
		File loc = new File(curLoc);
		if (!loc.exists()) {
			curLoc = locationLocal;
		} 
		
		loc = new File(curLoc);
		if (!loc.exists()) {
			curLoc = "D:/WORKSPACE/upload-dir"; // Folder Syamsu
		}
		
		String pathFile = curLoc + "/profile-" + getKdProfile() + "/" + namafile;
		
		File file = new File(pathFile);
		
		if (!file.exists()) {	
			
			pathFile = curLoc + "/profile-4/" + namafile;
			file = new File(pathFile);	
			
			if (!file.exists()) {				
			
				pathFile = curLoc + "/" + namafile;
				file = new File(pathFile);		
				
				if (!file.exists()) {
					//System.out.println("File gambar " + namafile + " di URL "+pathFile+" tidak ditemukan.");
					throw new InfoException("File " + pathFile+ " tidak ditemukan.");
				}
			}
		}
		
		
		
		try {
			return ImageIO.read(file);
		}catch(Exception e) {
			throw new InfoException("File gambar " + namafile + " tidak bisa dibaca, pastikan itu adalah file gambar.");
		}	
	}
	
	public Image getImageFromUrl(String urlfile) {
		
		File file = new File(urlfile);
		if (!file.exists()) {
			//System.out.println("File gambar di URL "+urlfile+" tidak ditemukan.");
			throw new InfoException("File "+urlfile+" tidak ditemukan.");
		}
		
		try {
			return ImageIO.read(file);
		}catch(Exception e) {
			throw new InfoException("File gambar " + urlfile + " tidak bisa dibaca, pastikan itu adalah file gambar.");
		}	
	}
}
