package com.portofolio.auth.service;

import java.util.Map;

import com.portofolio.base.BaseService;

public interface MenuDinamisVersionService extends BaseService {
	Map<String,Object> selectAllObjekAplikasi();
	Map<String,Object> selectAllInfoUrl();
}
