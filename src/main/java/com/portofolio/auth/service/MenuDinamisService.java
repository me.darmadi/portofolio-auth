package com.portofolio.auth.service;
import java.util.Map;

import com.portofolio.base.BaseService;

public interface MenuDinamisService extends BaseService {
	Map<String,Object> selectAllObjekAplikasi();
//	List<MenuDto> selectAwalObjekAplikasi();
//	List<MenuDto> selectLanjutObjekAplikasi(String kdObjekModulAplikasi);

	Map<String,Object> selectAllInfoUrl();
}
