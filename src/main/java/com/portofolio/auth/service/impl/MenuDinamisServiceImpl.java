package com.portofolio.auth.service.impl;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portofolio.auth.service.MenuDinamisService;
import com.portofolio.base.BaseServiceImpl;
import com.portofolio.dao.ObjekModulAplikasiDao;
import com.portofolio.dto.SessionDto;
import com.portofolio.entity.ObjekModulAplikasi;
import com.portofolio.util.CommonUtil;

@Service("MenuDinamisService")
public class MenuDinamisServiceImpl extends BaseServiceImpl implements MenuDinamisService {
//	@Autowired
//	AppMenuDinamicMapper appMenuDinamicMapper;
//	
	@Autowired
	ObjekModulAplikasiDao objekModulAplikasiDao;

	@Override
	public Map<String, Object> selectAllInfoUrl() {
//		if (kdKelompokUser == 1) {
//			
//		} else {
//			
//		}
		return null;
	}
	
	private List<Map<String,Object>> selectAllObjekAplikasi(Integer kdProfile, String kdModulAplikasi, Integer kdKelompokUser, String KdObjekModulAplikasiHead, Map<String,Object> listurl, Map<String,Object> namaObjek) {
		List<ObjekModulAplikasi> objekmodulaplikasiS;
		if (kdKelompokUser == 1) {
			objekmodulaplikasiS = (KdObjekModulAplikasiHead == null) ?
					objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuAdmin(kdProfile, kdModulAplikasi):
					objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenuAdmin(kdProfile, kdModulAplikasi, KdObjekModulAplikasiHead);			
		} else {
			objekmodulaplikasiS = (KdObjekModulAplikasiHead == null) ?
					objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenu(kdProfile, kdModulAplikasi, kdKelompokUser):
					objekModulAplikasiDao.findAllObjekModulAplikasiDaofindMenu(kdProfile, kdModulAplikasi, kdKelompokUser, KdObjekModulAplikasiHead);
		}
		
		List<Map<String,Object>> menuutama = CommonUtil.createList();
		Map<String,Object> menu;
		
		for (ObjekModulAplikasi objekModulAplikasi:objekmodulaplikasiS) {

			List<Map<String,Object>> items = selectAllObjekAplikasi(kdProfile, kdModulAplikasi, kdKelompokUser, objekModulAplikasi.getKode(), listurl, namaObjek);
					
			if (CommonUtil.isNullOrEmpty(items)) {
				String link = objekModulAplikasi.getAlamatURLFormObjek().replaceAll("#", "");
				String label = objekModulAplikasi.getNamaObjekModulAplikasi();
				listurl.put(link, 1);
				namaObjek.put(label, link);
				
				if (objekModulAplikasi.getIsMenu() != null && objekModulAplikasi.getIsMenu().equals(1)) {
					menu = CommonUtil
							.initMap()
							.put("label", objekModulAplikasi.getNamaObjekModulAplikasi())			
							.put("icon", "fa fa-fw fa-chevron-right")
							.put("routerLink", new String[] {link})
							.getMap();
				} else {
					continue;
				}
			} else {
				menu = CommonUtil
						.initMap()
						.put("label", objekModulAplikasi.getNamaObjekModulAplikasi())			
						.put("icon", "fa fa-fw fa-bars")
						.put("items", items)
						.getMap();
			}
			
			menuutama.add(menu);
		}
		
		return menuutama;
	}
	
	@Override
	public Map<String,Object> selectAllObjekAplikasi() {
		SessionDto sess = getSession();
		Map<String,Object> listUrl = CommonUtil.createMap();
		Map<String,Object> namaObjek = CommonUtil.createMap();
		listUrl.put("/", 1);
		List<Map<String,Object>> menuUtama = selectAllObjekAplikasi(sess.getKdProfile(), sess.getKdModulAplikasi(), sess.getKdKelompokUser(),  null, listUrl, namaObjek);
		return CommonUtil.initMap().put("menuUtama",menuUtama).put("listUrl", listUrl).getMap();
	}

}
