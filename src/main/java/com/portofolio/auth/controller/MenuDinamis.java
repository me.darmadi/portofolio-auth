//package com.portofolio.auth.controller;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.portofolio.auth.service.MenuDinamisService;
//import com.portofolio.auth.service.MenuDinamisVersionService;
//import com.portofolio.util.CommonUtil;
//
///**
//*
//*/
//@RestController
//@RequestMapping("/menu")
//public class MenuDinamis {
//
//	@Autowired 
//	MenuDinamisVersionService menuDinamisService;
//	
//	protected Map<String, String> mapHeaderMessage = new HashMap<String, String>();
//	
//	@GetMapping("/dinamic")
//	@ResponseBody
//	public Map<String,Object> getMenuDinamic(){
//		return CommonUtil.initMap().put("data", menuDinamisService.selectAllObjekAplikasi()).getMap();			
//	}
//	
//	@GetMapping("/info-url")
//	@ResponseBody
//	public Map<String,Object> getInfoUrl(){
//		return CommonUtil.initMap().put("data", menuDinamisService.selectAllInfoUrl()).getMap();			
//	}
//	
//}
