package com.portofolio.auth.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.portofolio.constant.BaseConstant;
import com.portofolio.constant.SecurityConstant;
import com.portofolio.dao.mapper.AuthMapper;
import com.portofolio.dao.provider.AuthProvider;
import com.portofolio.domain.ModulAplikasi;
import com.portofolio.domain.Profile;
import com.portofolio.domain.Ruangan;
import com.portofolio.dto.AuthDto;
import com.portofolio.dto.LoginDto;
import com.portofolio.dto.PasienDto;
import com.portofolio.dto.SessionDto;
import com.portofolio.dto.UserDto;
import com.portofolio.exception.InfoException;
import com.portofolio.security.AppPermission;
import com.portofolio.security.TokenAuthenticationService;
import com.portofolio.service.LoginUserService;
import com.portofolio.util.AuthUtil;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.RestUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import io.swagger.annotations.ApiOperation;

/**
 * Controller class for Authenticate Business
 * 
 * @author Adik / Syamsu
 */
@RestController
@RequestMapping("/auth")
public class AuthenticateController {
	
	@Autowired
	AuthMapper authMapper;
	
	@Autowired
	LoginUserService loginUserService;
	
	@ApiOperation(value = "Api Untuk Find versi LanguageManager by lang, kdModulAplikasi" )
	@GetMapping("/sendNotif")
	@ResponseBody
	public Map<String, Object> sendNotif(){
		
		Map<String,Object> map = CommonUtil.initMap()
				.put("data",  CommonUtil.initMap()
						.put("KdPegawai", "00305")
						.put("KdProfile", 3)
						.put("NamaPegawai", "Syamsu Rizal Ali")
						.put("status", "is Checked In")
						.put("StatusAbsensi", 1)
						.put("NamaDepartemen", "Pusat")
						.getMap())
				.put("type", 2)
				.getMap();
		
		try {
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost("hris.bottis.co.id");
			factory.setPort(5672);
			factory.setUsername("rsab");
			factory.setPassword("rsab");
			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			String mExchange = "amq.topic";
			channel.exchangeDeclare(mExchange, "topic", true);
			String mQueue = channel.queueDeclare().getQueue();
			channel.queueBind(mQueue, mExchange, "absensi.3.00305");
			channel.basicPublish(mExchange, "absensi.3.00305", MessageProperties.TEXT_PLAIN, new Gson().toJson(map).getBytes());
				
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		return CommonUtil.createMap();
	}

	

	protected Map<String, String> mapHeaderMessage = CommonUtil.createMap();

	@Autowired
	public TokenAuthenticationService tokenAuthenticationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticateController.class);

	@RequestMapping(value = "/sign-in/set-ruangan", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> signInSetRuangan(@Valid @RequestBody AuthDto authDto, HttpServletRequest request,
			HttpServletResponse httpResponse) {

		authDto.setEncrypted("AUIEO");
		AuthDto authSetProfile = loginUserService.listModulAplikasi(authDto);

		LOGGER.warn("/sign-in/set-ruangan");

		mapHeaderMessage.clear();

		if (authSetProfile == null) {
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.UNAUTHORIZED.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, HttpStatus.UNAUTHORIZED.toString());
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
			return RestUtil.getJsonResponse(null, HttpStatus.UNAUTHORIZED, mapHeaderMessage);
		} else {
			Map<String,Object> result=new HashMap<String, Object>();
			if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdRuangan())) {
				authSetProfile=loginUserService.listModulAplikasi(authSetProfile);
				if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdModulAplikasi())) {
					Map<String, Object> grand = AuthUtil.GrandAuth(authSetProfile, loginUserService, tokenAuthenticationService, httpResponse);
					if (grand != null) {
						mapHeaderMessage.put(SecurityConstant.HEADER_X_AUTH_TOKEN, (String) grand.get("token"));
						result=loginUserService.getResult(authSetProfile);
					}
				}else{
					result=loginUserService.resultModulAplikasi(authSetProfile);
				}
			}else if(CommonUtil.isNullOrEmpty(authSetProfile.getModulAplikasis())) {
				throw new InfoException("User Belum Memiliki ModulAplikasi");
			}else{
				result=loginUserService.resultModulAplikasi(authSetProfile);
			}
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
			return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
		}

	}
	
	
	@RequestMapping(value = "/sign-in/set-lokasi", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> signInSetLokasi(@Valid @RequestBody AuthDto authDto, HttpServletRequest request,
			HttpServletResponse httpResponse) {

		if (authDto.getKdLokasi() == null) {
			return RestUtil.getJsonHttptatus(HttpStatus.BAD_REQUEST);
		}
		authDto.setEncrypted("AUIEO");
		AuthDto authSetProfile = loginUserService.listRuangan(authDto);
		
		mapHeaderMessage.clear();

		LOGGER.warn("/sign-in/set-lokasi");

		if (authSetProfile == null) {
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.UNAUTHORIZED.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, HttpStatus.UNAUTHORIZED.toString());
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
			return RestUtil.getJsonResponse(null, HttpStatus.UNAUTHORIZED, mapHeaderMessage);
		} else {
			Map<String,Object> result=new HashMap<String, Object>();
			if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdRuangan())) {
				authSetProfile=loginUserService.listModulAplikasi(authSetProfile);
				if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdModulAplikasi())) {
					Map<String, Object> grand = AuthUtil.GrandAuth(authSetProfile, loginUserService, tokenAuthenticationService, httpResponse);
					if (grand != null) {
						mapHeaderMessage.put(SecurityConstant.HEADER_X_AUTH_TOKEN, (String) grand.get("token"));
						result=loginUserService.getResult(authSetProfile);
					}
				}else{
					result=loginUserService.resultModulAplikasi(authSetProfile);
				}
			}else if(CommonUtil.isNullOrEmpty(authSetProfile.getRuangans())) {
				throw new InfoException("User Belum Memiliki Ruangan");
			}else {
				result=loginUserService.resultRuangan(authSetProfile);
			}
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
			return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
		}

	}

	@RequestMapping(value = "/sign-in/set-modul-aplikasi", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> signInModulAplikasi(@Valid @RequestBody AuthDto authDto, HttpServletRequest request,
			HttpServletResponse httpResponse) {

		if (authDto.getKdRuangan() == null) {
			return RestUtil.getJsonHttptatus(HttpStatus.BAD_REQUEST);
		}

		AuthDto authSetProfile = loginUserService.authorize(authDto);

		mapHeaderMessage.clear();

		LOGGER.warn("/sign-in/set-modul-aplikasi");

		if (authSetProfile == null) {
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.UNAUTHORIZED.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, HttpStatus.UNAUTHORIZED.toString());
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
			return RestUtil.getJsonResponse(null, HttpStatus.UNAUTHORIZED, mapHeaderMessage);
		} else {
			Map<String,Object> result=new HashMap<String, Object>();
			authSetProfile=loginUserService.listRuangan(authSetProfile);
			if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdRuangan())) {
				authSetProfile=loginUserService.listModulAplikasi(authSetProfile);
				if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdModulAplikasi())) {
					Map<String, Object> grand = AuthUtil.GrandAuth(authSetProfile, loginUserService, tokenAuthenticationService, httpResponse);
					if (grand != null) {
						result=loginUserService.getResult(authSetProfile);
						mapHeaderMessage.put(SecurityConstant.HEADER_X_AUTH_TOKEN, (String) grand.get("token"));
					}
				}else if(CommonUtil.isNullOrEmpty(authSetProfile.getModulAplikasis())) {
					throw new InfoException("User Belum Memiliki Modul Aplikasi");
				}else{
					result=loginUserService.resultModulAplikasi(authSetProfile);
				}
			}else {
				result=loginUserService.resultRuangan(authSetProfile);
			}
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
			return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
		}
	}
	
	
	@RequestMapping(value = "/sign-in/set-home", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> signInSetHome(@Valid @RequestBody AuthDto authDto, HttpServletRequest request,
			HttpServletResponse httpResponse) {


		AuthDto authSetProfile = loginUserService.authorize(authDto);

		mapHeaderMessage.clear();

		LOGGER.warn("/sign-in/set-home");

		if (authSetProfile == null) {
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.UNAUTHORIZED.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, HttpStatus.UNAUTHORIZED.toString());
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
			return RestUtil.getJsonResponse(null, HttpStatus.UNAUTHORIZED, mapHeaderMessage);
		} else {
			Map<String,Object> result=new HashMap<String, Object>();
			if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdModulAplikasi())) {
				Map<String, Object> grand = AuthUtil.GrandAuth(authSetProfile, loginUserService, tokenAuthenticationService, httpResponse);
				if (grand != null) {
					result=loginUserService.getResult(authSetProfile);
					mapHeaderMessage.put(SecurityConstant.HEADER_X_AUTH_TOKEN, (String) grand.get("token"));
				}
			}else {
				throw new InfoException("user belum Memiliki Modul Aplikasi");
			}
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
			return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
		}
	}

	@RequestMapping(value = "/sign-in/set-profile", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> signInProfile(@Valid @RequestBody AuthDto authDto, HttpServletRequest request,
			HttpServletResponse httpResponse) {

		authDto.setEncrypted("AUIEO");
		AuthDto authSetProfile = loginUserService.listLokasi(authDto);
		
		mapHeaderMessage.clear();

		LOGGER.warn("/sign-in/set-profile");

		if (authSetProfile == null) {
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.UNAUTHORIZED.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, HttpStatus.UNAUTHORIZED.toString());
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
			return RestUtil.getJsonResponse(null, HttpStatus.UNAUTHORIZED, mapHeaderMessage);
		} else {
			Map<String,Object> result=new HashMap<String, Object>();
			if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdLokasi())) {
				authSetProfile=loginUserService.listRuangan(authSetProfile);
				if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdRuangan())) {
					authSetProfile=loginUserService.listModulAplikasi(authSetProfile);
					if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdModulAplikasi())) {
						Map<String, Object> grand = AuthUtil.GrandAuth(authSetProfile, loginUserService, tokenAuthenticationService, httpResponse);
						if (grand != null) {
							result=loginUserService.getResult(authSetProfile);
							mapHeaderMessage.put(SecurityConstant.HEADER_X_AUTH_TOKEN, (String) grand.get("token"));
						}
					}else{
						result=loginUserService.resultModulAplikasi(authSetProfile);
					}
				}else {
					result=loginUserService.resultRuangan(authSetProfile);
				}
			
			}else if(CommonUtil.isNullOrEmpty(authSetProfile.getLokasis())) {
				throw new InfoException("User Belum Memiliki Lokasi");
			}else {
				result=loginUserService.resultLokasi(authSetProfile);
			}
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
			return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
		}

	}
	

	@RequestMapping(value = "/sign-in/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> signInUser(@Valid @RequestBody LoginDto loginDto, HttpServletRequest request,
			HttpServletResponse httpResponse) {

		if (loginDto.getNamaUser() == null || loginDto.getKataSandi() == null) {
			return RestUtil.getJsonHttptatus(HttpStatus.BAD_REQUEST);
		}

		AuthDto authDtoU = loginUserService.signIn(loginDto);

		mapHeaderMessage.clear();

		LOGGER.warn("/sign-in/login");

		if (authDtoU == null) {
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.UNAUTHORIZED.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, HttpStatus.UNAUTHORIZED.toString());
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_UNSUCCESSFUL);
			return RestUtil.getJsonResponse(null, HttpStatus.UNAUTHORIZED, mapHeaderMessage);
		} else {

			Map<String,Object> result=new HashMap<String, Object>();
			AuthDto authSetProfile=authDtoU;
			if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdProfile())) {
				authSetProfile =loginUserService.listLokasi(authSetProfile);
				if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdLokasi())) {
					authSetProfile=loginUserService.listRuangan(authSetProfile);
					if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdRuangan())) {
						authSetProfile=loginUserService.listModulAplikasi(authSetProfile);
						if(CommonUtil.isNotNullOrEmpty(authSetProfile.getKdModulAplikasi())) {
							Map<String, Object> grand = AuthUtil.GrandAuth(authSetProfile, loginUserService, tokenAuthenticationService, httpResponse);
							if (grand != null) {
								result=loginUserService.getResult(authSetProfile);
								mapHeaderMessage.put(SecurityConstant.HEADER_X_AUTH_TOKEN, (String) grand.get("token"));
							}
						}else{
							result=loginUserService.resultModulAplikasi(authSetProfile);
						}
					}else if(CommonUtil.isNullOrEmpty(authSetProfile.getRuangans())) {
						throw new InfoException("User Belum Mempunyai Ruangan");
					}else {
						result=loginUserService.resultRuangan(authSetProfile);
					}
				}else {
					result=loginUserService.resultLokasi(authSetProfile);
				}
			}else if(CommonUtil.isNullOrEmpty(authSetProfile.getProfiles())) {
				throw new InfoException("User Belum Mempunyai Profile");
			}
			else {
				result=loginUserService.resultProfile(authSetProfile);
			}
		
			mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
			mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
			mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
			return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
		}
	}


	
}
