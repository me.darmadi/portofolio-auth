package com.portofolio.auth.controller;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portofolio.constant.BaseConstant;
import com.portofolio.util.CommonUtil;
import com.portofolio.util.RestUtil;

@RestController
@RequestMapping("/off")
public class AuthOffController {

	protected Map<String, String> mapHeaderMessage = CommonUtil.createMap();
	
	
	@GetMapping("/sign-out-all")
	public ResponseEntity<Map<String,Object>>  signOutPasien() {
		Map<String, Object> result = CommonUtil.createMap();
		result.put("logout", "success");
		mapHeaderMessage.put(BaseConstant.STATUS, HttpStatus.OK.name());
		mapHeaderMessage.put(BaseConstant.STATUS_CODE, "200");
		mapHeaderMessage.put(BaseConstant.MESSAGE, BaseConstant.HttpHeaderInfo.REQUEST_SUCCESSFUL);
		return RestUtil.getJsonResponse(result, HttpStatus.OK, mapHeaderMessage);
	}
}
