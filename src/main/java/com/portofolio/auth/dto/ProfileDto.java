package com.portofolio.auth.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileDto {
	Integer statusEnabled;
	Integer kdJenisProfile;
	String kdDepartemen;
	String namaLengkap;
	String reportDisplay;
	String hostname1;
}
