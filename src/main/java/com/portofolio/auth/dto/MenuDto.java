package com.portofolio.auth.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
*
* @author >
*/
@Getter
@Setter
public class MenuDto extends ObjekModulAplikasiDto {
	
	private List<MenuDto> child;
	
}
