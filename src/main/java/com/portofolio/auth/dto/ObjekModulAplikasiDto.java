package com.portofolio.auth.dto;

import lombok.Getter;
import lombok.Setter;

/**
*
* @author >
*/
@Getter
@Setter
public class ObjekModulAplikasiDto {

	private String kdObjekModulAplikasi;
	private String kdObjekModulAplikasiHead;
	private String objekModulAplikasi;
	private String alamatURLFormObjek;
	private Integer noUrutObjek;
}
