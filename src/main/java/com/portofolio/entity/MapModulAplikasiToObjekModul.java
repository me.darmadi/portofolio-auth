/* --------------------------------------------------------
 Generated by Gunandi using sql2java (http://sql2java.sourceforge.net/ )
 jdbc driver used at code generation time: net.sourceforge.jtds.jdbc.Driver
 
 Author : Gunandi (andri.gunandi1@gmail.com) 
 Date Created : 22/05/2019
 --------------------------------------------------------
*/
package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.*;

import  javax.persistence.*;
import  javax.validation.constraints.*;

import  lombok.AllArgsConstructor;
import  lombok.Getter;
import  lombok.NoArgsConstructor;
import  lombok.Setter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.portofolio.entity.vo.MapModulAplikasiToObjekModulId;


@Entity
@Table(name = "MapModulAplikasiToObjekModul_S",indexes = {@Index(name = "MapModulAplikasiToObjekModul_S_Index",  columnList="statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class MapModulAplikasiToObjekModul {
    
    @EmbeddedId
    private MapModulAplikasiToObjekModulId id ;
    
    private static final String cDefNoUrut =INTEGER;
    @Column(name = "NoUrut", columnDefinition = cDefNoUrut)
    private Integer noUrut;
    
    private static final String cDefStatusEnabled =INTEGER;
    @Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
    @NotNull(message = "mapmodulaplikasitoobjekmodul.statusenabled.notnull")
    private Boolean statusEnabled;
    
    private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 36 + AKHIR_KURUNG ;
    @Column(name = "NoRec", columnDefinition = cDefNoRec)
    @NotNull(message = "mapmodulaplikasitoobjekmodul.norec.notnull")
    private String noRec;
    
    private static final String cDefVersion =INTEGER;
    @Version
    @Column(name = "version", nullable = false,  columnDefinition = cDefVersion)
    @NotNull(message = "mapmodulaplikasitoobjekmodul.version.notnull")
    private Integer version;
       
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdObjekModulAplikasi", referencedColumnName="KdObjekModulAplikasi",insertable=false, updatable=false),
    })
     private ObjekModulAplikasi objekModulAplikasi;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumns({
        @JoinColumn(name="KdModulAplikasi", referencedColumnName="KdModulAplikasi",insertable=false, updatable=false),
    })
     private ModulAplikasi modulAplikasi;
    

}
