package com.portofolio.entity;

import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AKHIR_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.AWAL_KURUNG;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.INTEGER;
import static com.portofolio.base.adapter.DatabaseAdapter.Constant.VARCHAR;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/***
 * 
 * @author Syamsu Rizal
 *
 */

@Entity
@Table(name = "ModulAplikasi_S")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor  @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ModulAplikasi {
	
	/*@EmbeddedId
	private ModulAplikasiId id;*/
	private static final String cDefKdModulAplikasi =  VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
    @Column(name = "KdModulAplikasi", columnDefinition = cDefKdModulAplikasi)
	@NotNull(message = "modulaplikasi.kdmodulaplikasi.notnull")
	@Id
	private String kode;
	
	private static final String cDefReportDisplay = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Size(max = 100, message = "ReportDisplay max. 100 character")
	@Column(name = "ReportDisplay", length = 100, columnDefinition = cDefReportDisplay)
	protected String reportDisplay;
	
	private static final String cDefKdModulAplikasiHead = VARCHAR + AWAL_KURUNG + 2 + AKHIR_KURUNG;
	@Column(name = "KdModulAplikasiHead", columnDefinition = cDefKdModulAplikasiHead)
	private String kdModulAplikasiHead;

	private static final String cDefModulIconImage = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "ModulIconImage", columnDefinition = cDefModulIconImage)
	private String modulIconImage;

	private static final String cDefModulNoUrut = INTEGER;
	@Column(name = "ModulNoUrut", columnDefinition = cDefModulNoUrut)
	private Integer modulNoUrut;

	private static final String cDefModulAplikasi = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "ModulAplikasi", columnDefinition = cDefModulAplikasi)
	@NotNull(message = "modulaplikasi.modulaplikasi.notnull")
	private String namaModulAplikasi;
	
	private static final String cDefVersion = INTEGER;
	@Version
	@Column(name = "version", nullable = false, columnDefinition = cDefVersion)
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;

	private static final String cDefStatusEnabled = INTEGER;// AWAL_KURUNG + 1 + AKHIR_KURUNG;
	@Column(name = "StatusEnabled", columnDefinition = cDefStatusEnabled)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public Boolean statusEnabled;

	private static final String cDefNoRec = VARCHAR + AWAL_KURUNG + 100 + AKHIR_KURUNG;
	@Column(name = "NoRec", columnDefinition = cDefNoRec)
	@NotNull(message = "NoRec Harus Diisi")
	private String noRec;

}
