ALTER TABLE JenisProfile_M NOCHECK CONSTRAINT ALL
SET IDENTITY_INSERT JenisProfile_M ON
INSERT INTO JenisProfile_M (KdJenisProfile, StatusEnabled, version, NoRec, JenisProfile, ReportDisplay) VALUES (1, 1, 1, NEWID(), 'Rumah Sakit', 'Rumah Sakit')
INSERT INTO JenisProfile_M (KdJenisProfile, StatusEnabled, version, NoRec, JenisProfile, ReportDisplay) VALUES (2, 1, 1, NEWID(), 'Teknologi Informasi', 'Teknologi Informasi')
INSERT INTO JenisProfile_M (KdJenisProfile, StatusEnabled, version, NoRec, JenisProfile, ReportDisplay) VALUES (3, 1, 1, NEWID(), 'Data Center', 'Data Center')
INSERT INTO JenisProfile_M (KdJenisProfile, StatusEnabled, version, NoRec, JenisProfile, ReportDisplay) VALUES (4, 1, 1, NEWID(), 'Perhotelan', 'Perhotelan')
INSERT INTO JenisProfile_M (KdJenisProfile, StatusEnabled, version, NoRec, JenisProfile, ReportDisplay) VALUES (5, 1, 1, NEWID(), 'Perbankan', 'Perbankan')
INSERT INTO JenisProfile_M (KdJenisProfile, StatusEnabled, version, NoRec, JenisProfile, ReportDisplay) VALUES (6, 1, 1, NEWID(), 'Pabrik / Industri', 'Industri / Industri')
SET IDENTITY_INSERT JenisProfile_M OFF
ALTER TABLE JenisProfile_M CHECK CONSTRAINT ALL

