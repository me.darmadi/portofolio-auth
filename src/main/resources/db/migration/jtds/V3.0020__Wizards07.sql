ALTER TABLE SatuanStandar_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 1, '01', NULL, NULL, 'Satuan Standar Aset', NEWID(), 'Satuan Standar Aset', 1, 1);
	INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 2, '01', NULL, 1, 'mL', NEWID(), 'mL', 1, 1);
	INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 3, '01', NULL, 1, 'Cc', NEWID(), 'Cc', 1, 1);
    INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 4, '01', NULL, NULL, 'Satuan Range', NEWID(), 'Satuan Range', 1, 1);
	INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 5, '01', NULL, 4, 'Hari', NEWID(), 'Hari', 1, 1);
    INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 6, '01', NULL, 4, 'Bulan', NEWID(), 'Bulan', 1, 1);
	INSERT INTO SatuanStandar_M (KdProfile, KdSatuanStandar, KdDepartemen, KdKelompokProduk, KdSatuanStandarHead, SatuanStandar, NoRec, ReportDisplay, StatusEnabled, version) VALUES (0, 7, '01', NULL, 4, 'Tahun', NEWID(), 'Tahun', 1, 1);
GO
ALTER TABLE SatuanStandar_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE Range_M NOCHECK CONSTRAINT ALL
GO 
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 1, NULL, '01', 18,NULL, NULL, '', '', 'Range Keterlambatan', NEWID(), 1, 'X', NULL, 16, 'Range Keterlambatan', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 2, NULL, '01', 6, NULL, NULL, '', '','Range Jam Ke-1 Hari Normal', NEWID(), 1, 'X', 60, 0, 'Range Jam Ke-1 Hari Normal', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 3, NULL, '01', 6, NULL, NULL, '', '','Range Jam Ke-2 Hari Normal', NEWID(), 2, 'X', 120, 61, 'Range Jam Ke-2 Hari Normal', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 4, NULL, '01', 6, NULL, NULL, '', '','Range Jam Ke-3 Hari Normal', NEWID(), 3, 'X', 180, 121, 'Range Jam Ke-3 Hari Normal', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 5, NULL, '01', 6, NULL, NULL, '', '','Range Jam Ke-4 Hari Normal', NEWID(), 4, 'X', 240, 181, 'Range Jam Ke-4 Hari Normal', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 6, NULL, '01', 6, NULL, NULL, '', '','Lembur Lumpsum Hari Normal', NEWID(), 5, 'X',  NULL, 120, 'Lembur Lumpsum Hari Normal', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 7, NULL, '01', 7, NULL, NULL, '', '','Range Jam Ke-8 Hari Off', NEWID(), 1, 'X', 480, 0, 'Range Jam Ke-8 Hari Off', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 8, NULL, '01', 7, NULL, NULL, '', '','Range Jam Ke-9 Hari Off', NEWID(), 2, 'X', 540, 481, 'Range Jam Ke-9 Hari Off', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 9, NULL, '01', 7, NULL, NULL, '', '','Range Jam Ke-10 Hari Off', NEWID(), 3, 'X', 600, 541, 'Range Jam Ke-10 Hari Off', 1, 1);
	INSERT INTO Range_M (KdProfile, KdRange, FactorRate, KdDepartemen, KdJenisRange, KdSatuanStandar, KdTypeDataObjek, KodeExternal, NamaExternal, NamaRange, NoRec, NoUrut, OperatorFactorRate, RangeMax, RangeMin, ReportDisplay, StatusEnabled, version)  VALUES (0, 10, NULL, '01', 7,NULL, NULL, '', '', 'Lembur Lumpsum Hari Libur', NEWID(), 4, 'X', 600, 120, 'Lembur Lumpsum Hari Libur', 1, 1);
	GO
ALTER TABLE Range_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE KelompokShiftKerja_M NOCHECK CONSTRAINT ALL
GO 
	INSERT INTO KelompokShiftKerja_M (KdProfile,KdKelompokShift,FactorRate,KdDepartemen,KodeExternal,NamaExternal,KelompokShift,NoRec,OperatorFactorRate,ReportDisplay,StatusEnabled,version) VALUES (0,1,1,'01',NULL,'Non Shift','Non Shift',NEWID(),'X','Non Shift',1,1);
	INSERT INTO KelompokShiftKerja_M (KdProfile,KdKelompokShift,FactorRate,KdDepartemen,KodeExternal,NamaExternal,KelompokShift,NoRec,OperatorFactorRate,ReportDisplay,StatusEnabled,version) VALUES (0,2,1,'01',NULL,'Shift','Shift',NEWID(),'X','Shift',1,1);
	GO
ALTER TABLE KelompokShiftKerja_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE JENISTRANSAKSI_M NOCHECK CONSTRAINT ALL
GO 
	INSERT INTO JENISTRANSAKSI_M (KdProfile, KdJenisTransaksi, JenisPersenCito, KdDepartemen, KdKelasDefault, KdKelompokPelayanan, KdProdukCito, KdProdukDeposit, KdProdukRetur, KodeExternal, MetodeAmbilHargaNetto, MetodeHargaNetto, MetodeStokHargaNetto, NamaExternal, JenisTransaksi, NoRec, ReportDisplay, SistemDiscount, SistemHargaNetto, StatusEnabled, TglBerlakuTarif, version) VALUES (0, 1, 1, '01', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '', 'Transaksi Medical', NEWID(), 'Transaksi Medical', NULL, 1, 1, NULL, 1);
	INSERT INTO JENISTRANSAKSI_M (KdProfile, KdJenisTransaksi, JenisPersenCito, KdDepartemen, KdKelasDefault, KdKelompokPelayanan, KdProdukCito, KdProdukDeposit, KdProdukRetur, KodeExternal, MetodeAmbilHargaNetto, MetodeHargaNetto, MetodeStokHargaNetto, NamaExternal, JenisTransaksi, NoRec, ReportDisplay, SistemDiscount, SistemHargaNetto, StatusEnabled, TglBerlakuTarif, version) VALUES (0, 2, 1, '01', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '', 'Transaksi Perjalanan Dinas', NEWID(), 'Transaksi Perjalanan Dinas', NULL, 1, 1, NULL, 1);
	GO
ALTER TABLE JENISTRANSAKSI_M CHECK  CONSTRAINT ALL
GO