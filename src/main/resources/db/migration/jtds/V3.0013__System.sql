ALTER TABLE Departemen_M NOCHECK CONSTRAINT ALL
ALTER TABLE Profile_M NOCHECK CONSTRAINT ALL
SET IDENTITY_INSERT Profile_M ON 
INSERT INTO Profile_M(KdProfile, KdJenisProfile, Hostname1, NPWP, NamaLengkap, NoRec, ReportDisplay, version)
VALUES(0, 1, 'internal', '', 'Internal', NEWID(), 'Internal', 1)
INSERT INTO Departemen_M (KdProfile, KdDepartemen, NamaDepartemen, ReportDisplay, NoRec, version, StatusEnabled) VALUES (0, '01', '', '', '', 1, 1)
SET IDENTITY_INSERT Profile_M OFF
ALTER TABLE Profile_M CHECK CONSTRAINT ALL
ALTER TABLE Departemen_M CHECK CONSTRAINT ALL
 
ALTER TABLE JenisKelamin_M NOCHECK CONSTRAINT ALL
ALTER TABLE KelompokPegawai_M NOCHECK CONSTRAINT ALL
ALTER TABLE JenisPegawai_M NOCHECK CONSTRAINT ALL
ALTER TABLE KelompokUser_S NOCHECK CONSTRAINT ALL 
ALTER TABLE MapLoginUserToPegawai_S NOCHECK CONSTRAINT ALL
ALTER TABLE MapLoginUserToProfile_S NOCHECK CONSTRAINT ALL
ALTER TABLE MapObjekModulToKelompokUser_S NOCHECK CONSTRAINT ALL
ALTER TABLE TypeDataObjek_S NOCHECK CONSTRAINT ALL
GO
	INSERT INTO TypeDataObjek_S (KdTypeDataObjek, StatusEnabled, version, NoRec, ReportDisplay, NamaTypeDataObjek) VALUES (1, 1, 1, NEWID(), 'Varchar', 'Varchar')
	INSERT INTO TypeDataObjek_S (KdTypeDataObjek, StatusEnabled, version, NoRec, ReportDisplay, NamaTypeDataObjek) VALUES (2, 1, 1, NEWID(), 'Integer', 'Integer')
	INSERT INTO TypeDataObjek_S (KdTypeDataObjek, StatusEnabled, version, NoRec, ReportDisplay, NamaTypeDataObjek) VALUES (3, 1, 1, NEWID(), 'Double', 'Varchar')
	
    INSERT INTO KelompokPegawai_M (KdProfile, KdKelompokPegawai, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, KelompokPegawai) VALUES (0, '0', 1, 1, '01', NEWID(), 'Administrator', 'Administrator')
	INSERT INTO KelompokPegawai_M (KdProfile, KdKelompokPegawai, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, KelompokPegawai) VALUES (0, '1', 1, 1, '01', NEWID(), 'Keluarga Pegawai', 'Keluarga Pegawai')
	INSERT INTO KelompokPegawai_M (KdProfile, KdKelompokPegawai, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, KelompokPegawai) VALUES (0, '2', 1, 1, '01', NEWID(), 'Referensi', 'Referensi')
	
	INSERT INTO JenisPegawai_M (KdProfile, KdJenisPegawai, StatusEnabled, version, KdDepartemen, NoRec, KdKelompokPegawai, ReportDisplay, JenisPegawai) VALUES (0, '00', 1, 1, '01', NEWID(), '0', 'Keluarga Pegawai', 'Keluarga Pegawai')
	INSERT INTO JenisPegawai_M (KdProfile, KdJenisPegawai, StatusEnabled, version, KdDepartemen, NoRec, KdKelompokPegawai, ReportDisplay, JenisPegawai) VALUES (0, '01', 1, 1, '01', NEWID(), '1', 'Administrator', 'Administrator')
	INSERT INTO JenisPegawai_M (KdProfile, KdJenisPegawai, StatusEnabled, version, KdDepartemen, NoRec, KdKelompokPegawai, ReportDisplay, JenisPegawai) VALUES (0, '03', 1, 1, '01', NEWID(), '1', 'Dokter', 'Dokter')
	
	INSERT KelompokUser_S (KdProfile, KdKelompokUser, NoRec, StatusEnabled, version, KdDepartemen, KelompokUser, ReportDisplay) VALUES (0, 1, NEWID(), 1, 1, '01', 'Administrator','Administrator')
	INSERT KelompokUser_S (KdProfile, KdKelompokUser, NoRec, StatusEnabled, version, KdDepartemen, KelompokUser, ReportDisplay) VALUES (0, 2, NEWID(), 1, 1, '01', 'Operator','Operator')
	INSERT KelompokUser_S (KdProfile, KdKelompokUser, NoRec, StatusEnabled, version, KdDepartemen, KelompokUser, ReportDisplay) VALUES (0, 3, NEWID(), 1, 1, '01', 'User','User')

	INSERT MapObjekModulToKelompokUser_S (KdProfile, KdKelompokUser, KdObjekModulAplikasi, NoRec, StatusEnabled, version, Cetak, Hapus, Simpan, Tampil, Ubah) VALUES (0, 2, '000402', NEWID(), 1, 1, 0, 0, 0, 0, 0)
GO
ALTER TABLE TypeDataObjek_S CHECK  CONSTRAINT ALL;
ALTER TABLE KelompokPegawai_M CHECK CONSTRAINT ALL
ALTER TABLE JenisPegawai_M CHECK CONSTRAINT ALL
ALTER TABLE KelompokUser_S CHECK CONSTRAINT ALL 
ALTER TABLE MapLoginUserToPegawai_S CHECK CONSTRAINT ALL
ALTER TABLE MapLoginUserToProfile_S CHECK CONSTRAINT ALL
ALTER TABLE MapObjekModulToKelompokUser_S CHECK CONSTRAINT ALL