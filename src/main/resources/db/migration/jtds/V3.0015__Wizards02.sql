ALTER TABLE JenisAlamat_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO JenisAlamat_M (KdProfile, KdJenisAlamat, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisAlamat) VALUES (0, 1, 1, 1, '01', NEWID(), 'KTP', 'KTP')
	INSERT INTO JenisAlamat_M (KdProfile, KdJenisAlamat, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisAlamat) VALUES (0, 2, 1, 1, '01', NEWID(), 'Tempat Tinggal', 'Tempat Tinggal')
	INSERT INTO JenisAlamat_M (KdProfile, KdJenisAlamat, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisAlamat) VALUES (0, 3, 1, 1, '01', NEWID(), 'Tambahan', 'Tambahan')
	INSERT INTO JenisAlamat_M (KdProfile, KdJenisAlamat, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisAlamat) VALUES (0, 4, 1, 1, '01', NEWID(), 'Kantor', 'Kantor')
GO
ALTER TABLE JenisAlamat_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE TitlePegawai_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 1, 1, 1, '01', NEWID(), 'Tn.', 'Tn.')
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 2, 1, 1, '01', NEWID(), 'Ny.', 'Ny.')
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 3, 1, 1, '01', NEWID(), 'Nn.', 'Nn.')
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 4, 1, 1, '01', NEWID(), 'dr.', 'dr.')
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 5, 1, 1, '01', NEWID(), 'Dr.', 'Dr.')
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 6, 1, 1, '01', NEWID(), 'Ir.', 'Ir.')
	INSERT INTO TitlePegawai_M (KdProfile, KdTitle, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, NamaTitle) VALUES (0, 7, 1, 1, '01', NEWID(), 'Drs.', 'Drs.')
GO
ALTER TABLE TitlePegawai_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE JenisRange_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  1, 1, 1, '01', NEWID(), 'Hasil Rekrutmen', 'Hasil Rekrutmen')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  2, 1, 1, '01', NEWID(), 'Range Masa Kerja', 'Range Masa Kerja')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  3, 1, 1, '01', NEWID(), 'Range Pajak Penghasilan', 'Range Pajak Penghasilan')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  4, 1, 1, '01', NEWID(), 'Range Pinjaman', 'Range Pinjaman')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  5, 1, 1, '01', NEWID(), 'Range Waktu Pinjaman', 'Range Waktu Pinjaman')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  6, 1, 1, '01', NEWID(), 'Lembur Hari Kerja Normal', 'Lembur Hari Kerja Normal')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  7, 1, 1, '01', NEWID(), 'Lembur Hari Off/Libur Nasional', 'Lembur Hari Off/Libur Nasional')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  8, 1, 1, '01', NEWID(), 'No Range', 'No Range')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0,  9, 1, 1, '01', NEWID(), 'Range Masa Kerja Gaji', 'Range Masa Kerja Gaji')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 10, 1, 1, '01', NEWID(), 'Range Group Harga', 'Range Group Harga')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 12, 1, 1, '01', NEWID(), 'Range Masa Kerja Pinjaman (Tahun)', 'Range Masa Kerja Pinjaman (Tahun)')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 13, 1, 1, '01', NEWID(), 'Range Masa Kerja Cuti (Bulan)', 'Range Masa Kerja Cuti (Bulan)')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 14, 1, 1, '01', NEWID(), 'Range Masa Kerja Pensiun (Tahun)', 'Range Masa Kerja Pensiun (Tahun)')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 15, 1, 1, '01', NEWID(), 'Range Masa Kerja PHK (Tahun)', 'Range Masa Kerja PHK (Tahun)')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 16, 1, 1, '01', NEWID(), 'Range Masa Kerja Reimburs (Tahun)', 'Range Masa Kerja Reimburs (Tahun)')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 17, 1, 1, '01', NEWID(), 'Range Masa Kerja THR (Tahun)', 'Range Masa Kerja THR (Tahun)')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 18, 1, 1, '01', NEWID(), 'Range Keterlambatan', 'Range Keterlambatan')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 19, 1, 1, '01', NEWID(), 'Masa Kerja Kategory', 'Masa Kerja Kategory')
	INSERT INTO JenisRange_M (KdProfile, KdJenisRange, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisRange) VALUES (0, 20, 1, 1, '01', NEWID(), 'Range Masa Kerja Gaji BPJS', 'Range Masa Kerja Gaji BPJS')
GO
ALTER TABLE JenisRange_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE JenisKeputusan_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO JenisKeputusan_M (KdProfile, KdJenisKeputusan, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisKeputusan) VALUES (0, 1, 1, 1, '01', NEWID(), 'Disetujui', 'Disetujui')
	INSERT INTO JenisKeputusan_M (KdProfile, KdJenisKeputusan, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, JenisKeputusan) VALUES (0, 2, 1, 1, '01', NEWID(), 'Tidak Disetujui','Tidak Disetujui')
GO
ALTER TABLE JenisKeputusan_M CHECK  CONSTRAINT ALL
GO
