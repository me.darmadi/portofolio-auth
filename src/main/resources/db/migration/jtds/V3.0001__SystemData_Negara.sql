ALTER TABLE JenisDokumen_M NOCHECK CONSTRAINT ALL

INSERT INTO MDM5.JenisDokumen_M (KdProfile,KdJenisDokumen,KdDepartemen,KdJenisDokumenHead,KodeExternal,NamaExternal,JenisDokumen,NoRec,ReportDisplay,RetensiDokumenTahun,StatusEnabled,version) VALUES 
(4,16,'01',18,NULL,NULL,'Sertifikat 1','3178faad-d42e-468f-a9dc-48b70601e166','Sertifikat 1',NULL,1,5)
,(4,17,'01',NULL,NULL,NULL,'Transkrip Nilai','1c5f9746-10e4-49fc-95b2-6e9a05323141','Transkrip Nilai',NULL,1,6)
,(4,18,'01',18,NULL,NULL,'Sertifikat 7','63514302-c7a0-4894-9695-3467dff7dc3f','Sertifikat 7',NULL,1,5)
,(4,19,'01',NULL,'','','KITAS','c15b2380-ab62-4bd8-8888-c188790b4937','KITAS',NULL,1,1)
,(4,20,'01',NULL,'','','Sertifikat 5','cd6b7c82-0001-426d-99f1-8398c84df3bd','Sertifikat 5',NULL,1,1)
,(4,21,'01',NULL,'','','Sertifikat 6','f4a8916d-6b4b-4223-bc67-7bf159ab80ad','Sertifikat 6',NULL,1,1)
,(4,22,'01',NULL,'','','NPWP','6b6a5b03-c4e7-420c-9a43-e3fdceb8f380','NPWP',NULL,1,1)
,(4,23,'01',NULL,'','','Sertifikat 2','39b82d18-6234-4c61-a055-297f3cd731f6','Sertifikat 2',NULL,1,1)
,(4,24,'01',NULL,'','','Formulir Pengajuan Pinjaman','421b962a-4701-4b8a-9007-3c2d762d78e3','Formulir Pengajuan Pinjaman',NULL,1,1)
,(4,25,'01',NULL,'','','Perjanjian Pinjaman Uang Karyawan','46c3e324-0e94-4345-b3fe-77d6abd1b604','Perjanjian Pinjaman Uang Karyawan',NULL,0,2)
;

ALTER TABLE JenisDokumen_M CHECK  CONSTRAINT ALL
