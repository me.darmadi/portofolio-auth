ALTER TABLE PAKET_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO PAKET_M (KdProfile, KdPaket, KodeExternal, NamaExternal, NoRec, ReportDisplay, StatusEnabled, version, KdDepartemen, KdJenisPaket, KdJenisTransaksi, NamaPaket) VALUES(0, 1, '', '', NEWID(), 'Perjalanan Dinas Staff', 1, 1, '01', 2, 2, 'Perjalanan Dinas Staff');
	INSERT INTO PAKET_M (KdProfile, KdPaket, KodeExternal, NamaExternal, NoRec, ReportDisplay, StatusEnabled, version, KdDepartemen, KdJenisPaket, KdJenisTransaksi, NamaPaket) VALUES(0, 2, '', '', NEWID(), 'Perjalanan Dinas Manager', 1, 1, '01', 2, 2, 'Perjalanan Dinas Manager');
	INSERT INTO PAKET_M (KdProfile, KdPaket, KodeExternal, NamaExternal, NoRec, ReportDisplay, StatusEnabled, version, KdDepartemen, KdJenisPaket, KdJenisTransaksi, NamaPaket) VALUES(0, 3, '', '', NEWID(), 'Paket Medical Rawat Jalan', 1, 1, '01', 2, 1, 'Paket Medical Rawat Jalan');
	INSERT INTO PAKET_M (KdProfile, KdPaket, KodeExternal, NamaExternal, NoRec, ReportDisplay, StatusEnabled, version, KdDepartemen, KdJenisPaket, KdJenisTransaksi, NamaPaket) VALUES(0, 4, '', '', NEWID(), 'Paket Medical Rawat Inap', 1, 1, '01', 2, 1, 'Paket Medical Rawat Inap');
GO
ALTER TABLE PAKET_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE JENISASET_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO JENISASET_M (KdProfile, KdJenisAset, KdAccount, KdDepartemen, KdJenisAsetHead, JenisAset, NoRec, ReportDisplay, StatusEnabled, version) VALUES(0, 1, NULL, '01', NULL, 'Tanah', NEWID(), 'Tanah', 1, 1);
	INSERT INTO JENISASET_M (KdProfile, KdJenisAset, KdAccount, KdDepartemen, KdJenisAsetHead, JenisAset, NoRec, ReportDisplay, StatusEnabled, version) VALUES(0, 2, NULL, '01', NULL, 'Kendaraan', NEWID(), 'Kendaraan', 1, 1);
	INSERT INTO JENISASET_M (KdProfile, KdJenisAset, KdAccount, KdDepartemen, KdJenisAsetHead, JenisAset, NoRec, ReportDisplay, StatusEnabled, version) VALUES(0, 3, NULL, '01', NULL, 'Bangunan', NEWID(), 'Bangunan', 1, 1);
GO
ALTER TABLE JENISASET_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE KATEGORYASET_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO KATEGORYASET_M (KdProfile, KdKategoryAset, KdDepartemen, KdKategoryAsetHead, NamaKategoryAset, NoRec, ReportDisplay, StatusEnabled, version) VALUES(0, 1, '01', NULL, 'Aset Operasional', NEWID(), 'Aset Operasional', 1, 1);
	INSERT INTO KATEGORYASET_M (KdProfile, KdKategoryAset, KdDepartemen, KdKategoryAsetHead, NamaKategoryAset, NoRec, ReportDisplay, StatusEnabled, version) VALUES(0, 2, '01', NULL, 'Aset Non Operasional', NEWID(), 'Aset Non Operasional', 1, 1);
GO
ALTER TABLE KATEGORYASET_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE KELOMPOKASET_M NOCHECK CONSTRAINT ALL
GO
	INSERT KELOMPOKASET_M (KdProfile, KdKelompokAset, KdAccount, KdDepartemen, KdJenisAset, KdMetodePenyusutan, KelompokAset, NoRec, PersenPenyusutan, ReportDisplay, RumusPenyusutan, StatusEnabled, UmurEkonomisThn, version) VALUES (0, 1, NULL, '01', 1, NULL, 'Tanah', NEWID(), NULL, 'Tanah', NULL, 1, 10, 1);
	INSERT KELOMPOKASET_M (KdProfile, KdKelompokAset, KdAccount, KdDepartemen, KdJenisAset, KdMetodePenyusutan, KelompokAset, NoRec, PersenPenyusutan, ReportDisplay, RumusPenyusutan, StatusEnabled, UmurEkonomisThn, version) VALUES (0, 2, NULL, '01', 2, NULL, 'Motor', NEWID(), NULL, 'Motor', NULL, 1, 5, 1);
	INSERT KELOMPOKASET_M (KdProfile, KdKelompokAset, KdAccount, KdDepartemen, KdJenisAset, KdMetodePenyusutan, KelompokAset, NoRec, PersenPenyusutan, ReportDisplay, RumusPenyusutan, StatusEnabled, UmurEkonomisThn, version) VALUES (0, 3, NULL, '01', 2, NULL, 'Mobil', NEWID(), NULL, 'Mobil', NULL, 1, 7, 1);
	INSERT KELOMPOKASET_M (KdProfile, KdKelompokAset, KdAccount, KdDepartemen, KdJenisAset, KdMetodePenyusutan, KelompokAset, NoRec, PersenPenyusutan, ReportDisplay, RumusPenyusutan, StatusEnabled, UmurEkonomisThn, version) VALUES (0, 4, NULL, '01', 3, NULL, 'Bangunan', NEWID(), NULL, 'Bangunan', NULL, 1, 20, 1);
	GO
ALTER TABLE KATEGORYASET_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE JenisRekanan_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO JenisRekanan_M (KdProfile, KdJenisRekanan, KodeExternal, NamaExternal, NoRec, ReportDisplay, StatusEnabled, version, KdDepartemen, JenisRekanan) VALUES (0, '1', NULL, NULL, NEWID(), 'Perusahaan Asuransi', '1', '1', '01', 'Perusahaan Asuransi');
	INSERT INTO JenisRekanan_M (KdProfile, KdJenisRekanan, KodeExternal, NamaExternal, NoRec, ReportDisplay, StatusEnabled, version, KdDepartemen, JenisRekanan) VALUES (0, '2', NULL, NULL, NEWID(), 'KPP', '1', '1', '01', 'KPP');
GO
ALTER TABLE JenisRekanan_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE REKANAN_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO REKANAN_M (KdProfile, KdRekanan, ContactPerson, GambarLogo, KdAccount, KdDepartemen, KdJenisRekanan, KdPegawai, KdRekananHead, KodeExternal, NPWP, NamaExternal, NamaRekanan, NoPKP, NoRec, ReportDisplay, StatusEnabled, TglAkhir, TglAwal, TglDaftar, version) VALUES (0, 2, NULL, NULL, NULL, '01', 1, NULL, NULL, NULL, NULL, NULL, 'PT. BPJS Kesehatan', NULL, NEWID(), 'PT. BPJS Kesehatan', 1, 0, 0, 1451581200, 1);
	INSERT INTO REKANAN_M (KdProfile, KdRekanan, ContactPerson, GambarLogo, KdAccount, KdDepartemen, KdJenisRekanan, KdPegawai, KdRekananHead, KodeExternal, NPWP, NamaExternal, NamaRekanan, NoPKP, NoRec, ReportDisplay, StatusEnabled, TglAkhir, TglAwal, TglDaftar, version) VALUES (0, 3, NULL, NULL, NULL, '01', 1, NULL, NULL, NULL, NULL, NULL, 'PT. BPJS Ketenagakerjaan', NULL, NEWID(), 'PT. BPJS Ketenagakerjaan', 1, 0, 0, 1451581200, 1);
	GO
ALTER TABLE REKANAN_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE MapAngkaToBulan_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '1', '1', 'Januari', '01', '', '', NEWID(), '1', 'Januari', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '2', '2', 'Februari', '01', '', '', NEWID(), '2', 'Febuari', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '3', '3', 'Maret', '01', '', '', NEWID(), '3', 'Maret', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '4', '4', 'April', '01', '', '', NEWID(), '4', 'April', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '5', '5', 'Mei', '01', '', '', NEWID(), '5', 'Mei', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '6', '6', 'Juni', '01', '', '', NEWID(), '6', 'Juni', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '7', '7', 'Juli', '01', '', '', NEWID(), '7', 'Juli', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '8', '8', 'Agustus', '01', '', '', NEWID(), '8', 'Agustus', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '9', '9', 'September', '01', '', '', NEWID(), '9', 'September', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '10', '10', 'Oktober', '01', '', '', NEWID(), '10', 'Oktober', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '11', '11', 'November', '01', '', '', NEWID(), '11', 'November', '1', '1');
	INSERT INTO MapAngkaToBulan_M ( KdProfile, KdMapping, FormatAngka, FormatBulan, KdDepartemen, KodeExternal, NamaExternal, NoRec, NoUrut, ReportDisplay, StatusEnabled, version) VALUES (0, '12', '12', 'Desember', '01', '', '', NEWID(), '12', 'Desember', '1', '1');
GO
ALTER TABLE MapAngkaToBulan_M CHECK  CONSTRAINT ALL
GO

