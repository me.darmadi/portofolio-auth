ALTER TABLE KelompokTransaksi_M NOCHECK CONSTRAINT ALL
GO
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 1, NEWID(), 'NIK pegawai', 1, 1, null,  'NIK pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 2, NEWID(), 'Orientasi', 1, 1, 0, 'Orientasi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 3, NEWID(), 'Absensi Pegawai', 1, 1, null, 'Absensi Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 4, NEWID(), 'Perubahan Jadwal Kerja', 1, 1, null, 'Perubahan Jadwal Kerja')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 5, NEWID(), 'Pengajuan Perjalanan Dinas', 1, 1, 0, 'Pengajuan Perjalanan Dinas')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 6, NEWID(), 'Data Histori Pendidikan', 1, 1, null, 'Data Histori Pendidikan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 7, NEWID(), 'Data Histori Organisasi', 1, 1, null, 'Data Histori Organisasi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 8, NEWID(), 'Data Histori Pekerjaan Pegawai', 1, 1, null, 'Data Histori Pekerjaan Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES ( 9, NEWID(), 'Data Histori Dokumen Pegawai', 1, 1, null, 'Data Histori Dokumen Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (10, NEWID(), 'Data Histori Property Pegawai', 1, 1, null, 'Data Histori Property Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (11, NEWID(), 'Cuti Pegawai', 1, 1, null, 'Cuti Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (12, NEWID(), 'Resign Pegawai', 1, 1, null, 'Resign Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (13, NEWID(), 'Reimburs', 1, 1, 0, 'Reimburs')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (14, NEWID(), 'Gaji Pegawai', 1, 1, 0, 'Gaji Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (15, NEWID(), 'Lembur', 1, 1, 0, 'Lembur')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (16, NEWID(), 'Perhitungan Pajak', 1, 1, 0, 'Perhitungan Pajak')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (17, NEWID(), 'Perhitungan Asuransi', 1, 1, 0, 'Perhitungan Asuransi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (18, NEWID(), 'Pengajuan Reward', 1, 1, 0, 'Pengajuan Reward')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (19, NEWID(), 'Pengajuan Sanksi', 1, 1, 0, 'Pengajuan Sanksi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (20, NEWID(), 'Pengajuan Pinjaman', 1, 1, 1, 'Pengajuan Pinjaman')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (21, NEWID(), 'Pegawai Histori Kelompok Kerja', 1, 1, null, 'Pegawai Histori Kelompok Kerja')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (22, NEWID(), 'Data Histori Reward', 1, 1, 1, 'Data Histori Reward')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (23, NEWID(), 'Data Histori Offence', 1, 1, 1, 'Data Histori Offence')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (24, NEWID(), 'Data Histori Pegawai Meninggal', 1, 1, 1, 'Data Histori Pegawai Meninggal')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (25, NEWID(), 'Data Histori Pemeriksaan', 1, 1, 1, 'Data Histori Pemeriksaan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (26, NEWID(), 'Data Histori Keluarga', 1, 1, 1, 'Data Histori Keluarga')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (27, NEWID(), 'Evaluasi Jabatan', 1, 1, 1, 'Evaluasi Jabatan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (28, NEWID(), 'Mutasi Pegawai', 1, 1, 1, 'Mutasi Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (29, NEWID(), 'SK Fasilitas', 1, 1, 1, 'SK Fasilitas')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (30, NEWID(), 'SK Cuti Pegawai', 1, 1, 0, 'SK Cuti Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (31, NEWID(), 'SK Pinjaman', 1, 1, 0, 'SK Pinjaman')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (32, NEWID(), 'SK Restrukturisasi Gaji', 1, 1, 0, 'SK Restrukturisasi Gaji')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (33, NEWID(), 'Data Histori Visi', 1, 1, 0, 'Data Histori Visi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (34, NEWID(), 'Data Histori Misi', 1, 1, 0, 'Data Histori Misi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (35, NEWID(), 'Data Histori Tujuan', 1, 1, 0, 'Data Histori Tujuan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (36, NEWID(), 'Data Histori Slogan', 1, 1, 0, 'Data Histori Slogan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (37, NEWID(), 'Data Histori Slogan', 1, 1, 0, 'Data Histori Slogan')

GO
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (38, NEWID(), 'SK Karir Path', 1, 1, 1, 'SK Karir Path')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (39, NEWID(), 'SK Asuransi', 1, 1, 1, 'SK Asuransi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (40, NEWID(), 'Data Histori Slogan', 1, 1, 0, 'Data Histori Slogan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (41, NEWID(), 'Data Histori Slogan', 1, 1, 0, 'Data Histori Slogan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (42, NEWID(), 'Data Histori Slogan', 1, 1, 0, 'Data Histori Slogan')
GO
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (43, NEWID(), 'Perhitungan Cicilan', 1, 1, 0, 'Perhitungan Cicilan')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (44, NEWID(), 'Hitung Lembur', 1, 1, 0, 'Hitung Lembur')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (45, NEWID(), 'SK Potong Absen', 1, 1, 1, 'SK Potong Absen')
GO
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (62, NEWID(), 'Data Histori UMK', 1, 1, 0, 'Data Histori UMK')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (63, NEWID(), 'SK Profile Komponen Gaji', 1, 1, 0, 'SK Profile Komponen Gaji')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (64, NEWID(), 'SK Struktur Gaji By GP', 1, 1, 0, 'SK Struktur Gaji By GP')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (65, NEWID(), 'SK Struktur Gaji By JEP', 1, 1, 0, 'SK Struktur Gaji By JEP')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (66, NEWID(), 'SK Struktur Gaji By JLT', 1, 1, 0, 'SK Struktur Gaji By JLT')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (67, NEWID(), 'SK Struktur Gaji By JP', 1, 1, 0, 'SK Struktur Gaji By JP')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (68, NEWID(), 'SK Struktur Gaji By GPMKP', 1, 1, 0, 'SK Struktur Gaji By GPMKP')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (69, NEWID(), 'SK Struktur Gaji By MKRH', 1, 1, 0, 'SK Struktur Gaji By MKRH')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (70, NEWID(), 'Histori Job Deskripsi', 1, 1, 0, 'Histori Job Deskripsi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (71, NEWID(), 'Perhitungan Kebutuhan Pegawai', 1, 1, 0, 'Perhitungan Kebutuhan Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (72, NEWID(), 'Pengajuan Kebutuhan Pegawai', 1, 1, 0, 'Pengajuan Kebutuhan Pegawai')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (73, NEWID(), 'Penilaian Kompetensi', 1, 1, 0, 'Penilaian Kompetensi')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (74, NEWID(), 'Anggaran Biaya Job Posting', 1, 1, 0, 'Anggaran Biaya Job Posting')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (75, NEWID(), 'Job Posting', 1, 1, 0, 'Job Posting')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (76, NEWID(), 'Registrasi Pelamar', 1, 1, 0, 'Registrasi Pelamar')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (77, NEWID(), 'SK Reimburs', 1, 1, 0, 'SK Reimburs')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (78, NEWID(), 'SK PTKP', 1, 1, 0, 'SK PTKP')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (79, NEWID(), 'SK Pajak', 1, 1, 0, 'SK Pajak')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (80, NEWID(), 'Perhitungan BPJS TK', 1, 1, 0, 'Perhitungan BPJS TK')

INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (81, NEWID(), 'SK Pegawai Tidak Absen', 1, 1, null, 'SK Pegawai Tidak Absen')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (82, NEWID(), 'PHK', 1, 1, 0, 'PHK')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (83, NEWID(), 'SK Bonus', 1, 1, 0, 'SK Bonus')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (84, NEWID(), 'SK THR', 1, 1, 0, 'SK THR')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (85, NEWID(), 'SK Rapel', 1, 1, 0, 'SK Rapel')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (86, NEWID(), 'SK Gaji Pesangon', 1, 1, 0, 'SK Gaji Pesangon')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (87, NEWID(), 'Hitung Bonus', 1, 1, 0, 'Hitung Bonus')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (88, NEWID(), 'Hitung THR', 1, 1, 0, 'Hitung THR')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (89, NEWID(), 'Hitung Rapel', 1, 1, 0, 'Hitung Rapel')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (90, NEWID(), 'Hitung Gaji Pesangon', 1, 1, 0, 'Hitung Gaji Pesangon')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (91, NEWID(), 'Perubahan Pinjaman', 1, 1, 1, 'Perubahan Pinjaman')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (92, NEWID(), 'Hitung Paket Pensiun', 1, 1, 1, 'Hitung Paket Pensiun')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (93, NEWID(), 'Pembatalan Cuti', 1, 1, 1, 'Pembatalan Cuti')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (94, NEWID(), 'Pembatalan Dinas', 1, 1, 1, 'Pembatalan Dinas')
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (95, NEWID(), 'Pembatalan Lembur', 1, 1, 1, 'Pembatalan Lembur')

INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (96, NEWID(), 'Pengajuan Pensiun', 1, 1, NULL, 'Pengajuan Pensiun');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (97, NEWID(), 'Pegawai Tetap Awal', 1, 1, NULL, 'Pegawai Tetap Awal');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (98, NEWID(), 'Pengajuan Izin Atau Sakit', 1, 1, NULL, 'Pengajuan Izin Atau Sakit');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (99, NEWID(), 'BSC Visi Misi', 1, 1, NULL, 'BSC Visi Misi');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (100, NEWID(), 'BSC Strategy', 1, 1, NULL, 'BSC Strategy');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (101, NEWID(), 'BSC KPI', 1, 1, 0, 'BSC KPI');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (102, NEWID(), 'Data Histori Alamat', 1, 1, 0, 'Data Histori Alamat');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (103, NEWID(), 'Pemberian Penghargaan', 1, 1, 0, 'Pemberian Kesejahteraan');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (104, NEWID(), 'Pengajuan Realisasi KPI', 1, 1, 1, 'Pengajuan Realisasi KPI');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (105, NEWID(), 'Histori PTKP Pegawai', 1, 1, NULL, 'Histori PTKP Pegawai');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (106, NEWID(), 'SK Lock Pinjaman', 1, 1, NULL, 'SK Lock Pinjaman');
INSERT INTO KelompokTransaksi_M (KdKelompokTransaksi, NoRec, ReportDisplay, StatusEnabled, version, isCostInOut, KelompokTransaksi) VALUES (107, NEWID(), 'Pembatalan Transisi Karir', 1, 1, NULL, 'Pembatalan Transisi Karir');

GO
ALTER TABLE KelompokTransaksi_M CHECK  CONSTRAINT ALL
GO