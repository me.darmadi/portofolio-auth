ALTER TABLE Status_M NOCHECK CONSTRAINT ALL
GO
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0,  1, 1, 1, '01', NEWID(), NULL, 'Status Perkawinan', 'Status Perkawinan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0,  2, 1, 1, '01', NEWID(),    1, 'Lajang', 'Lajang')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0,  3, 1, 1, '01', NEWID(),    1, 'Menikah', 'Menikah')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0,  4, 1, 1, '01', NEWID(),    1, 'Cerai Mati', 'Cerai Mati')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0,  5, 1, 1, '01', NEWID(),    1, 'Cerai Hidup', 'Cerai Hidup')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 12, 1, 1, '01', NEWID(), NULL, 'Status Akreditasi', 'Status Akreditasi')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 13, 1, 1, '01', NEWID(),   12, 'Tidak Terakreditasi', 'Tidak Terakreditasi')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 14, 1, 1, '01', NEWID(),   12, 'Akreditasi Bersyarat', 'Akreditasi Bersyarat')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 15, 1, 1, '01', NEWID(),   12, 'Akreditasi Penuh', 'Akreditasi Penuh')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 16, 1, 1, '01', NEWID(), NULL, 'Status Pegawai', 'Status Pegawai')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 17, 1, 1, '01', NEWID(),   16, 'Aktif', 'Aktif')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 18, 1, 1, '01', NEWID(),   16, 'Cuti', 'Cuti')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 19, 1, 1, '01', NEWID(),   16, 'Pindah Kerja', 'Pindah Kerja')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 20, 1, 1, '01', NEWID(),   16, 'Pensiun', 'Pensiun')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 21, 1, 1, '01', NEWID(),   16, 'Resign', 'Resign')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 22, 1, 1, '01', NEWID(),   16, 'Meninggal', 'Meninggal')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 23, 1, 1, '01', NEWID(), NULL, 'Status Absensi', 'Status Absensi')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 24, 1, 1, '01', NEWID(),   23, 'Masuk', 'Masuk')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 25, 1, 1, '01', NEWID(),   23, 'Istirahat', 'Istirahat')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 26, 1, 1, '01', NEWID(),   23, 'Keluar', 'Keluar')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 27, 1, 1, '01', NEWID(),   23, 'Pulang', 'Pulang')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 28, 1, 1, '01', NEWID(),   23, 'Tidak Masuk', 'Tidak Masuk')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 29, 1, 1, '01', NEWID(), NULL, 'Status Kehadiran', 'Status Kehadiran')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 30, 1, 1, '01', NEWID(),   29, 'Perjalanan Dinas', 'Perjalanan Dinas')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 31, 1, 1, '01', NEWID(),   29, 'Perubahan Jadwal', 'Perubahan Jadwal')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 32, 1, 1, '01', NEWID(), NULL, 'Tipe Status', 'Tipe Status')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 33, 1, 1, '01', NEWID(),   32, 'Cuti Tahunan', 'Cuti Tahunan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 34, 1, 1, '01', NEWID(),   32, 'Cuti Besar', 'Cuti Besar')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 35, 1, 1, '01', NEWID(),   32, 'Cuti Melahirkan', 'Cuti Melahirkan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 36, 1, 1, '01', NEWID(),   32, 'Cuti Haid', 'Cuti Haid')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 37, 1, 1, '01', NEWID(),   32, 'Cuti Gugur Kandungan', 'Cuti Gugur Kandungan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 38, 1, 1, '01', NEWID(),   32, 'Cuti Ibadah', 'Cuti Ibadah')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 39, 0, 1, '01', NEWID(),   32, 'Cuti Pengganti', 'Cuti Pengganti')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 40, 1, 1, '01', NEWID(),   32, 'Cuti Karyawan Menikah', 'Cuti Karyawan Menikah')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 41, 1, 1, '01', NEWID(),   32, 'Cuti Menikahkan Anak', 'Cuti Menikahkan Anak')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 42, 1, 1, '01', NEWID(),   32, 'Cuti Mengkhitankan Anak', 'Cuti Mengkhitankan Anak')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 43, 1, 1, '01', NEWID(),   32, 'Cuti Istri Karyawan Melahirkan', 'Cuti Istri Karyawan Melahirkan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 44, 1, 1, '01', NEWID(),   32, 'Cuti Kematian Kel Terdekat', 'Cuti Kematian Kel Terdekat')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 45, 1, 1, '01', NEWID(),   32, 'Cuti Musibah Alam','Cuti Musibah Alam')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 46, 0, 1, '01', NEWID(),   32, 'Izin', 'Izin')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 47, 0, 1, '01', NEWID(),   32, 'Sakit', 'Sakit')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 48, 1, 1, '01', NEWID(), NULL, 'Jenis Lembur', 'Jenis Lembur')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 49, 1, 1, '01', NEWID(),   48, 'Lembur Hari Kerja Normal', 'Lembur Hari Kerja Normal')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 50, 1, 1, '01', NEWID(),   48, 'Lembur Hari Off/Libur Nasional', 'Lembur Hari Off/Libur Nasional')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 51, 1, 1, '01', NEWID(), NULL, 'Status Kepemilikan Aset','Status Kepemilikan Aset')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 52, 1, 1, '01', NEWID(), 51, 'Milik Profile','Milik Profile')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 53, 1, 1, '01', NEWID(), 51, 'Milik Pribadi','Milik Pribadi')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 54, 1, 1, '01', NEWID(), 51, 'Milik Rekanan','Milik Rekanan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 55, 1, 1, '01', NEWID(), 48, 'Keterlambatan','Keterlambatan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 56, 1, 1, '01', NEWID(), NULL, 'Status Over','Status Over')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 57, 1, 1, '01', NEWID(), 56, 'Ganti Hari','Ganti Hari')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 58, 1, 1, '01', NEWID(), 56, 'Dibayar','Dibayar')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 59, 1, 1, '01', NEWID(), NULL, 'Status PHK','Status PHK')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 60, 1, 1, '01', NEWID(), 59, 'PHK','PHK')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 61, 1, 1, '01', NEWID(), NULL, 'Status Pensiun','Status Pensiun')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 62, 1, 1, '01', NEWID(), 61, 'Pensiun Dini','Pensiun Dini')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 63, 1, 1, '01', NEWID(), 61, 'Di Pensiunkan','Di Pensiunkan')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 64, 1, 1, '01', NEWID(), 16, 'Tidak Aktif','Tidak Aktif')
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 65, 1, 1, '01', NEWID(), NULL, 'Status Absensi Tidak Masuk', 'Status Absensi Tidak Masuk');
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 66, 1, 1, '01', NEWID(), '65', 'Sakit Tanpa Surat', 'Sakit Tanpa Surat');
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 67, 1, 1, '01', NEWID(), '65', 'Sakit Dengan Surat', 'Sakit Dengan Surat');
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 68, 1, 1, '01', NEWID(), '65', 'Ijin', 'Ijin');
	INSERT INTO Status_M (KdProfile, KdStatus, StatusEnabled, version, KdDepartemen, NoRec, KdStatusHead, NamaStatus, ReportDisplay) VALUES (0, 69, 1, 1, '01', NEWID(), '65', 'Tanpa Keterangan', 'Tanpa Keterangan');
GO
ALTER TABLE Status_M CHECK  CONSTRAINT ALL
GO

ALTER TABLE KategoryDokumen_M NOCHECK  CONSTRAINT ALL
ALTER TABLE JenisDokumen_M NOCHECK  CONSTRAINT ALL
ALTER TABLE Dokumen_M NOCHECK  CONSTRAINT ALL
ALTER TABLE Pendidikan_M NOCHECK  CONSTRAINT ALL
ALTER TABLE PenghasilanTidakKenaPajak_M NOCHECK  CONSTRAINT ALL 
GO
	INSERT INTO KategoryDokumen_M (KdProfile, KdKategoryDokumen, StatusEnabled, version, KdDepartemen, NoRec, ReportDisplay, KategoryDokumen) VALUES (0, 1, 1, 1, '01', NEWID(), 'Soft Copy', 'Soft Copy')
	GO
	
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  1, 1, 1, '01', NEWID(), NULL, 'Identity Card', 'Identity Card')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  2, 1, 1, '01', NEWID(), NULL, 'CV', 'CV')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  3, 1, 1, '01', NEWID(),    1, 'KTP','KTP')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  4, 1, 1, '01', NEWID(),    1, 'KK', 'KK')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  5, 1, 1, '01', NEWID(),    1, 'KITAS', 'KITAS')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  6, 1, 1, '01', NEWID(),    1, 'Passport', 'Passport')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  7, 1, 1, '01', NEWID(),    1, 'SIM', 'SIM')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  8, 1, 1, '01', NEWID(), NULL, 'Surat', 'Surat')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  14, 1, 1, '01', NEWID(), NULL, 'Dokumen Aset', 'Dokumen Aset')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  15, 1, 1, '01', NEWID(), 14, 'SHM', 'SHM')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  16, 1, 1, '01', NEWID(), 14, 'Manual Book', 'Manual Book')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  17, 1, 1, '01', NEWID(), 14, 'STNK', 'STNK')
	INSERT INTO JenisDokumen_M (KdProfile, KdJenisDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdJenisDokumenHead, ReportDisplay, JenisDokumen) VALUES (0,  18, 1, 1, '01', NEWID(), 14, 'BPKB', 'BPKB')
	GO
	
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 1, 1, 1, '01', NEWID(), NULL, 1, 1, 1, '001', 'Identity Card',  'Kartu Identitas', 'Identity Card')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 2, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'KTP', 'Citizen Id',  'Citizen Id')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 3, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'Passport', 'Passport',  'Passport')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 4, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'Sim A', 'Driving License A', 'Driving License A')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 5, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'Sim B', 'Driving License B', 'Driving License B')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 6, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'Sim C', 'Driving License C', 'Driving License C')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 7, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'KITAS', 'Kitas', 'Kitas')
	INSERT INTO Dokumen_M (KdProfile, KdDokumen, StatusEnabled, version, KdDepartemen, NoRec, KdDokumenHead, isDokumenInOutInt, KdJenisDokumen, KdKategoryDokumen, KdRuangan, ReportDisplay, DeskripsiDokumen, NamaJudulDokumen) VALUES (0, 8, 1, 1, '01', NEWID(),    1, 1, 1, 1, '001', 'KK', 'Kartu Keluarga', 'Kartu Keluarga')
	GO
	
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '01', 1, 1, '01', NEWID(), 0, NULL,  'Level Pendidikan', 'Level Pendidikan')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '02', 1, 1, '01', NEWID(), 2,  '01', 'SD', 'SD')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '03', 1, 1, '01', NEWID(), 3,  '01', 'SLTP', 'SLTP')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '04', 1, 1, '01', NEWID(), 4,  '01', 'SLTA', 'SLTA')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '05', 1, 1, '01', NEWID(), 5,  '01', 'D1', 'D1')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '06', 1, 1, '01', NEWID(), 6,  '01', 'D2', 'D2')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '07', 1, 1, '01', NEWID(), 7,  '01', 'D3', 'D3')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '08', 1, 1, '01', NEWID(), 8,  '01', 'D4', 'D4')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '09', 1, 1, '01', NEWID(), 9,  '01', 'S1', 'S1')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '10', 1, 1, '01', NEWID(), 10,  '01', 'S2', 'S2')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '11', 1, 1, '01', NEWID(), 11,  '01', 'S3', 'S3')
	INSERT INTO Pendidikan_M (KdProfile, KdPendidikan, StatusEnabled, version, KdDepartemen, NoRec, NoUrut, KdPendidikanHead, ReportDisplay, Pendidikan) VALUES (0, '12', 1, 1, '01', NEWID(), 1,  '01', 'Belum Sekolah', 'Belum Sekolah')
	GO
	
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  1, 1, 1, '01', NEWID(),  54000000, 'Lajang, 0 Tanggungan', '-', 2, 0, 'Lajang, 0 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  2, 1, 1, '01', NEWID(),  58500000, 'Lajang, 1 Tanggungan', '-', 2, 1, 'Lajang, 1 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  3, 1, 1, '01', NEWID(),  63000000, 'Lajang, 2 Tanggungan', '-', 2, 2, 'Lajang, 2 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  4, 1, 1, '01', NEWID(),  67500000, 'Lajang, 3 Tanggungan', '-', 2, 3, 'Lajang, 3 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  5, 1, 1, '01', NEWID(),  58500000, 'Menikah, 0 Tanggungan','-', 3, 0, 'Menikah, 0 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  6, 1, 1, '01', NEWID(),  63000000, 'Menikah, 1 Tanggungan','-', 3, 1, 'Menikah, 1 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  7, 1, 1, '01', NEWID(),  67500000, 'Menikah, 2 Tanggungan','-', 3, 2, 'Menikah, 2 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  8, 1, 1, '01', NEWID(),  72000000, 'Menikah, 3 Tanggungan','-', 3, 3, 'Menikah, 3 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0,  9, 1, 1, '01', NEWID(), 112500000, 'Penghasilan Pasangan Digabung, 0 Tanggungan','-',3, 0, 'Penghasilan Pasangan Digabung, 0 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0, 10, 1, 1, '01', NEWID(), 117000000, 'Penghasilan Pasangan Digabung, 1 Tanggungan','-',3, 1, 'Penghasilan Pasangan Digabung, 1 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0, 11, 1, 1, '01', NEWID(), 121500000, 'Penghasilan Pasangan Digabung, 2 Tanggungan','-',3, 2, 'Penghasilan Pasangan Digabung, 2 Tanggungan')
	INSERT INTO PenghasilanTidakKenaPajak_M (KdProfile, KdPTKP, StatusEnabled, version, KdDepartemen, NoRec, TotalHargaPTKP, ReportDisplay, Deskripsi, KdStatusPerkawinan, QtyAnak, StatusPTKP) VALUES (0, 12, 1, 1, '01', NEWID(), 126000000, 'Penghasilan Pasangan Digabung, 3 Tanggungan','-',3, 3, 'Penghasilan Pasangan Digabung, 3 Tanggungan')
	GO 
ALTER TABLE KategoryDokumen_M CHECK  CONSTRAINT ALL
ALTER TABLE JenisDokumen_M CHECK  CONSTRAINT ALL
ALTER TABLE Dokumen_M CHECK  CONSTRAINT ALL
ALTER TABLE Pendidikan_M CHECK  CONSTRAINT ALL
ALTER TABLE PenghasilanTidakKenaPajak_M CHECK  CONSTRAINT ALL 
GO